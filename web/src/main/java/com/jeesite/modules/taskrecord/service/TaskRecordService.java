package com.jeesite.modules.taskrecord.service;

import java.util.Date;
import java.util.List;

import com.jeesite.modules.member.dao.MemberDao;
import com.jeesite.modules.points.dao.PointsDao;
import com.jeesite.modules.points.entity.Points;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.taskrecord.entity.TaskRecord;
import com.jeesite.modules.taskrecord.dao.TaskRecordDao;

/**
 * 任务记录表Service
 * @author han
 * @version 2023-11-01
 */
@Service
public class TaskRecordService extends CrudService<TaskRecordDao, TaskRecord> {
    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private MemberDao memberDao;
	/**
	 * 获取单条数据
	 * @param taskRecord
	 * @return
	 */
	@Override
	public TaskRecord get(TaskRecord taskRecord) {
		return super.get(taskRecord);
	}
	
	/**
	 * 查询分页数据
	 * @param taskRecord 查询条件
	 * @param taskRecord page 分页对象
	 * @return
	 */
	@Override
	public Page<TaskRecord> findPage(TaskRecord taskRecord) {
		return super.findPage(taskRecord);
	}
	
	/**
	 * 查询列表数据
	 * @param taskRecord
	 * @return
	 */
	@Override
	public List<TaskRecord> findList(TaskRecord taskRecord) {
		return super.findList(taskRecord);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param taskRecord
	 */
	@Override
	@Transactional
	public void save(TaskRecord taskRecord) {
		taskRecord.setExamineDate(new Date());
		super.save(taskRecord);
        if ("1".equals(taskRecord.getExamineState())) {
            Points points = new Points();
            points.setMemberCode(taskRecord.getMemberId());
            points.setPointsNum(taskRecord.getTaskPoints());
            points.setPointsType("0");
            points.setRelationId(taskRecord.getRecordId());
            pointsDao.insert(points);
            memberDao.updatePoints(points);
        }
	}
	
	/**
	 * 更新状态
	 * @param taskRecord
	 */
	@Override
	@Transactional
	public void updateStatus(TaskRecord taskRecord) {
		super.updateStatus(taskRecord);
	}
	
	/**
	 * 删除数据
	 * @param taskRecord
	 */
	@Override
	@Transactional
	public void delete(TaskRecord taskRecord) {
		taskRecord.sqlMap().markIdDelete(); // 逻辑删除时标记ID值
		super.delete(taskRecord);
	}


	/**
	 * 保存数据（插入或更新）
	 * @param taskRecord
	 */
	@Transactional
	public void insertTaskRecord(TaskRecord taskRecord) {
		super.insert(taskRecord);
	}
}