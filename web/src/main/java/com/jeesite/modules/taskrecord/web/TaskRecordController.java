package com.jeesite.modules.taskrecord.web;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.modules.general.CosUtil;
import com.jeesite.modules.sys.service.UserService;
import com.jeesite.modules.sys.utils.EmpUtils;
import com.jeesite.modules.sys.utils.UserUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.taskrecord.entity.TaskRecord;
import com.jeesite.modules.taskrecord.service.TaskRecordService;

/**
 * 任务记录表Controller
 * @author han
 * @version 2023-11-01
 */
@Controller
@RequestMapping(value = "${adminPath}/taskrecord/taskRecord")
public class TaskRecordController extends BaseController {

	@Autowired
	private TaskRecordService taskRecordService;
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TaskRecord get(String recordId, boolean isNewRecord) {
		return taskRecordService.get(recordId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("taskrecord:taskRecord:view")
	@RequestMapping(value = {"list", ""})
	public String list(TaskRecord taskRecord, Model model) {
		model.addAttribute("taskRecord", taskRecord);
		return "modules/taskrecord/taskRecordList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("taskrecord:taskRecord:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TaskRecord> listData(TaskRecord taskRecord, HttpServletRequest request, HttpServletResponse response) {
		taskRecord.setPage(new Page<>(request, response));
		Page<TaskRecord> page = taskRecordService.findPage(taskRecord);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("taskrecord:taskRecord:view")
	@RequestMapping(value = "form")
	public String form(TaskRecord taskRecord, Model model) {
		String imgUrl = taskRecord.getSubmitImg();
		List<String> lists = new ArrayList<>();
		if(!imgUrl.equals("")){
			String [] imgUrls=imgUrl.split(",");
			for(int i=0;i<imgUrls.length;i++){
				lists.add("'"+CosUtil.URL+imgUrls[i]+"'");
			}
			taskRecord.setSubmitImg(lists.toString());
		}
		model.addAttribute("taskRecord", taskRecord);
		return "modules/taskrecord/taskRecordForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("taskrecord:taskRecord:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TaskRecord taskRecord) {
     	taskRecord.setExamineId(UserUtils.getUser().getId());
		taskRecordService.save(taskRecord);
		return renderResult(Global.TRUE, text("保存任务记录表成功！"));
	}
	
}