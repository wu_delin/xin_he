package com.jeesite.modules.taskrecord.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.taskrecord.entity.TaskRecord;

/**
 * 任务记录表DAO接口
 * @author han
 * @version 2023-11-01
 */
@MyBatisDao
public interface TaskRecordDao extends CrudDao<TaskRecord> {
	
}