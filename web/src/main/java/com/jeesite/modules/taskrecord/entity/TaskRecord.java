package com.jeesite.modules.taskrecord.entity;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.PrimitiveIterator;

import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.sys.entity.User;
import com.jeesite.modules.task.entity.Task;

/**
 * 任务记录表Entity
 * @author han
 * @version 2023-11-01
 */
@Table(name="xh_task_record", alias="a", label="任务记录表信息", columns={
		@Column(name="record_id", attrName="recordId", label="记录id", isPK=true),
		@Column(name="task_id", attrName="taskId", label="任务id"),
		@Column(name="member_id", attrName="memberId", label="会员id"),
		@Column(name="examine_id", attrName="examineId", label="审核者id"),
		@Column(name="examine_date", attrName="examineDate", label="审核时间"),
		@Column(name="examine_state", attrName="examineState", label="审核状态"),
        @Column(name="examine_content", attrName="examineContent", label="审核内容"),
		@Column(name="submit_content", attrName="submitContent", label="提交内容"),
		@Column(name="submit_img", attrName="submitImg", label="提交照片"),
		@Column(name="status", attrName="status", label="节点状态", comment="节点状态", isUpdate=false),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false, isUpdateForce=true),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false, isUpdateForce=true),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
	},joinTable={
		@JoinTable(type=Type.LEFT_JOIN, entity= Task.class, alias="o",
			on="a.task_id = o.task_id", attrName="this",
			columns={
					@Column(name="task_points", attrName="taskPoints", label="任务积分"),
					@Column(name="task_type", attrName="taskType", label="任务类型"),
					@Column(name="task_name", attrName="taskName", label="任务名称"),
					@Column(name="start_date", attrName="startDate", label="开始日期"),
					@Column(name="end_date", attrName="endDate", label="结束日期"),
			 }),
		    @JoinTable(type=Type.LEFT_JOIN, entity= Member.class, alias="b",
				on="a.member_id = b.member_code", attrName="this",
				columns={
						@Column(name="member_name", attrName="memberName", label="党员姓名"),
						@Column(name="branch", attrName="branch", label="所属支部"),
				}),
        @JoinTable(type=Type.LEFT_JOIN, entity= User.class, alias="u",
                on="a.examine_id = u.user_code", attrName="this",
                columns={
                        @Column(name="ref_name", attrName="refName", label="审核者姓名", queryType=QueryType.LIKE),
                }),
		},
		orderBy="a.create_date DESC"
)
public class TaskRecord extends DataEntity<TaskRecord> {
	
	private static final long serialVersionUID = 1L;
	private String recordId;		// 记录id
	private String taskId;		    // 任务id
	private String taskPoints;		// 任务积分
    private String taskType;		// 任务类型
    private String taskName;		// 任务名称
    private String startDate;		// 开始日期
    private String endDate;		    // 截止日期

	private String memberId;		// 会员id
    private String memberName;      //党员姓名
    private String branch;          //所属支部
	private String examineId;		// 审核者id
	private String refName;		// 审核者
	private Date examineDate;		// 审核时间
	private String examineState;	// 审核状态
	private String submitContent;	// 提交内容
	private String submitImg;		// 提交照片
    private String examineContent;  // 审核内容


	public TaskRecord() {
		this(null);
	}
	
	public TaskRecord(String id){
		super(id);
	}

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getTaskPoints() {
        return taskPoints;
    }

    public void setTaskPoints(String taskPoints) {
        this.taskPoints = taskPoints;
    }

    public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	
	@Size(min=0, max=34, message="任务id长度不能超过 34 个字符")
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	@Size(min=0, max=34, message="会员id长度不能超过 34 个字符")
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Size(min=0, max=34, message="审核者id长度不能超过 34 个字符")
	public String getExamineId() {
		return examineId;
	}

	public void setExamineId(String examineId) {
		this.examineId = examineId;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getExamineDate() {
		return examineDate;
	}

	public void setExamineDate(Date examineDate) {
		this.examineDate = examineDate;
	}

	@Size(min=0, max=34, message="审核状态长度不能超过 34 个字符")
	public String getExamineState() {
		return examineState;
	}

	public void setExamineState(String examineState) {
		this.examineState = examineState;
	}
	
	@Size(min=0, max=256, message="提交内容长度不能超过 256 个字符")
	public String getSubmitContent() {
		return submitContent;
	}

	public void setSubmitContent(String submitContent) {
		this.submitContent = submitContent;
	}

	public String getSubmitImg() {
		return submitImg;
	}

	public void setSubmitImg(String submitImg) {
		this.submitImg = submitImg;
	}

	public String getExamineContent() {
		return examineContent;
	}

	public void setExamineContent(String examineContent) {
		this.examineContent = examineContent;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}