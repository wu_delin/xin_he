package com.jeesite.modules.points.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.points.entity.Points;
import com.jeesite.modules.points.dao.PointsDao;

/**
 * 积分记录Service
 * @author Mr Wu
 * @version 2023-11-07
 */
@Service
public class PointsService extends CrudService<PointsDao, Points> {
	
	/**
	 * 获取单条数据
	 * @param points
	 * @return
	 */
	@Override
	public Points get(Points points) {
		return super.get(points);
	}
	
	/**
	 * 查询分页数据
	 * @param points 查询条件
	 * @param points page 分页对象
	 * @return
	 */
	@Override
	public Page<Points> findPage(Points points) {
		return super.findPage(points);
	}
	
	/**
	 * 查询列表数据
	 * @param points
	 * @return
	 */
	@Override
	public List<Points> findList(Points points) {
		return super.findList(points);
	}
	
}