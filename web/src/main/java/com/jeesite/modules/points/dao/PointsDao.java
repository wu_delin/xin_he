package com.jeesite.modules.points.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.points.entity.Points;

/**
 * 积分记录DAO接口
 * @author Mr Wu
 * @version 2023-11-07
 */
@MyBatisDao
public interface PointsDao extends CrudDao<Points> {
	void removePoints(Points points);
}