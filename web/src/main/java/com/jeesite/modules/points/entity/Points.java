package com.jeesite.modules.points.entity;

import javax.validation.constraints.Size;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 积分记录Entity
 * @author Mr Wu
 * @version 2023-11-07
 */
@Table(name="xh_points", alias="a", label="积分记录信息", columns={
		@Column(name="record_id", attrName="recordId", label="积分id", isPK=true),
		@Column(name="member_code", attrName="memberCode", label="会员code"),
		@Column(name="points_num", attrName="pointsNum", label="积分分数"),
		@Column(name="points_type", attrName="pointsType", label="积分类型【0任务积分9负面扣分】"),
		@Column(name="relation_id", attrName="relationId", label="关联id"),
		@Column(name="status", attrName="status", label="0正常1删除", isUpdate=false),
		@Column(name="create_by", attrName="createBy", label="create_by", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="create_date", isUpdate=false, isQuery=false, isUpdateForce=true),
		@Column(name="update_by", attrName="updateBy", label="update_by", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="update_date", isQuery=false, isUpdateForce=true),
	}, orderBy="a.update_date DESC"
)
public class Points extends DataEntity<Points> {
	
	private static final long serialVersionUID = 1L;
	private String recordId;		// 积分id
	private String memberCode;		// 会员code
	private String pointsNum;		// 积分分数
	private String pointsType;		// 积分类型【0任务积分9负面扣分】
	private String relationId;		// 关联id

	public Points() {
		this(null);
	}
	
	public Points(String id){
		super(id);
	}
	
	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	
	@Size(min=0, max=34, message="会员code长度不能超过 34 个字符")
	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
	@Size(min=0, max=10, message="积分分数长度不能超过 10 个字符")
	public String getPointsNum() {
		return pointsNum;
	}

	public void setPointsNum(String pointsNum) {
		this.pointsNum = pointsNum;
	}
	
	@Size(min=0, max=1, message="积分类型【0任务积分9负面扣分】长度不能超过 1 个字符")
	public String getPointsType() {
		return pointsType;
	}

	public void setPointsType(String pointsType) {
		this.pointsType = pointsType;
	}
	
	@Size(min=0, max=34, message="关联id长度不能超过 34 个字符")
	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}
	
}