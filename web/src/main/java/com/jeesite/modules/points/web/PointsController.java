package com.jeesite.modules.points.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.modules.member.entity.Member;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.points.entity.Points;
import com.jeesite.modules.points.service.PointsService;

/**
 * 积分记录Controller
 * @author Mr Wu
 * @version 2023-11-07
 */
@Controller
@RequestMapping(value = "${adminPath}/points/points")
public class PointsController extends BaseController {

	@Autowired
	private PointsService pointsService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Points get(String recordId, boolean isNewRecord) {
		return pointsService.get(recordId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Points points, Model model) {
		model.addAttribute("points", points);
		return "modules/points/pointsList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Points> listData(Points points, HttpServletRequest request, HttpServletResponse response) {
		points.setPage(new Page<>(request, response));
		Page<Points> page = pointsService.findPage(points);
		return page;
	}

    /**
     * 积分管理
     */
    @RequiresPermissions("points:points:view")
    @RequestMapping(value = "pointsManage")
    public String pointsManage(Member member, Model model) {
        model.addAttribute("member", member);
        return "modules/points/pointsManage";
    }

}