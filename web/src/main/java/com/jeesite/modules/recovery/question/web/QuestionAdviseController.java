package com.jeesite.modules.recovery.question.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.question.entity.QuestionAdvise;
import com.jeesite.modules.recovery.question.service.QuestionAdviseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 问卷建议表Controller
 * @author Mr Wu
 * @version 2024-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/question/questionAdvise")
public class QuestionAdviseController extends BaseController {

	@Autowired
	private QuestionAdviseService questionAdviseService;
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "list")
	public String list(QuestionAdvise questionAdvise, Model model) {
		model.addAttribute("questionAdvise", questionAdvise);
		return "modules/recovery/question/questionRule";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public List<QuestionAdvise> listData(QuestionAdvise questionAdvise) {
        List<QuestionAdvise> list = questionAdviseService.findList(questionAdvise);
		return list;
	}

	/**
	 * 保存数据
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String saveData(QuestionAdvise questionAdvise) {
        questionAdviseService.saveData(questionAdvise);
		return renderResult(Global.TRUE, text("保存问卷建议表成功！"));
	}
	
}