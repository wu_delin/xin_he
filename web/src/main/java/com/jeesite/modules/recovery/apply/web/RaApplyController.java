package com.jeesite.modules.recovery.apply.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.apply.entity.RaApply;
import com.jeesite.modules.recovery.apply.service.RaApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 申请 康复治疗预约Controller
 * @author Mr Wu
 * @version 2025-02-17
 */
@Controller
@RequestMapping(value = "${adminPath}/recovery/apply")
public class RaApplyController extends BaseController {

	@Autowired
	private RaApplyService raApplyService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public RaApply get(String applyId, boolean isNewRecord) {
		return raApplyService.get(applyId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(RaApply raApply, Model model) {
		model.addAttribute("raApply", raApply);
		return "modules/recovery/apply/raApplyList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<RaApply> listData(RaApply raApply, HttpServletRequest request, HttpServletResponse response) {
		raApply.setPage(new Page<>(request, response));
		Page<RaApply> page = raApplyService.findPage(raApply);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(RaApply raApply, Model model) {
		model.addAttribute("raApply", raApply);
		return "modules/recovery/apply/raApplyForm";
	}

	/**
	 * 保存数据
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated RaApply raApply) {
		raApplyService.save(raApply);
		return renderResult(Global.TRUE, text("保存申请 康复治疗预约成功！"));
	}
	
	/**
	 * 删除数据
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(RaApply raApply) {
		raApplyService.delete(raApply);
		return renderResult(Global.TRUE, text("删除申请 康复治疗预约成功！"));
	}

}