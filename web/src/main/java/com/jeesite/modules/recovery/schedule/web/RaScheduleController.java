package com.jeesite.modules.recovery.schedule.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.alibaba.fastjson.JSONArray;
import com.jeesite.common.lang.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.schedule.entity.RaSchedule;
import com.jeesite.modules.recovery.schedule.service.RaScheduleService;
import java.util.ArrayList;
import java.util.List;

/**
 * 预约排班表Controller
 * @author Mr Wu
 * @version 2025-02-17
 */
@Controller
@RequestMapping(value = "${adminPath}/recovery/schedule")
public class RaScheduleController extends BaseController {

	@Autowired
	private RaScheduleService raScheduleService;

	String timePeriod = "[{'label':'12:30 ~ 13:00','value':'12:30 ~ 13:00'},{'label':'13:00 ~ 13:30','value':'13:00 ~ 13:30'}]";
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(RaSchedule raSchedule, Model model) {
		model.addAttribute("raSchedule", raSchedule);
        model.addAttribute("timePeriod", JSONArray.parseArray(timePeriod));
		return "modules/recovery/schedule/raScheduleList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<RaSchedule> listData(RaSchedule raSchedule, HttpServletRequest request, HttpServletResponse response) {
		raSchedule.setPage(new Page<>(request, response));
		Page<RaSchedule> page = raScheduleService.findPage(raSchedule);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "formBatch")
	public String formBatch(RaSchedule raSchedule, Model model) {
		model.addAttribute("raSchedule", raSchedule);
        model.addAttribute("timeList", JSONArray.parseArray(timePeriod));
		return "modules/recovery/schedule/raScheduleBatchForm";
	}

	/**
	 * 保存数据
	 */
    @PostMapping(value = "save")
    @ResponseBody
    public String save(RaSchedule raSchedule) {
        raScheduleService.save(raSchedule);
        return renderResult(Global.TRUE, text("更新康复治疗师成功！"));
    }
	@PostMapping(value = "saveBatch")
	@ResponseBody
	public String saveBatch(HttpServletRequest request) {
        String scheduleDate = request.getParameter("scheduleDate");
        List<RaSchedule> list = new ArrayList<>();
        int size = JSONArray.parseArray(timePeriod).size();
        for (int i = 0; i < size; i++) {
            RaSchedule raSchedule = new RaSchedule();
            raSchedule.setScheduleDate(DateUtils.parseDate(scheduleDate));
            raSchedule.setTimePeriod(request.getParameter("timePeriod"+(i+1)));
            raSchedule.setTherapistId(request.getParameter("therapist"+(i+1)+"_Id"));
            list.add(raSchedule);
        }
        raScheduleService.saveBatch(list);
		return renderResult(Global.TRUE, text("保存预约排班表成功！"));
	}
	
	/**
	 * 删除数据
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(RaSchedule raSchedule) {
	    if(null==raSchedule.getScheduleDate()){
            return renderResult(Global.FALSE, text("参数错误！"));
        }
		raScheduleService.delete(raSchedule);
		return renderResult(Global.TRUE, text("删除预约排班表成功！"));
	}

}