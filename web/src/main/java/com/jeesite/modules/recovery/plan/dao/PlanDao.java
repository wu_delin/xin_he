package com.jeesite.modules.recovery.plan.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.plan.entity.Plan;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 方案管理DAO接口
 * @author qu
 * @version 2025-02-21
 */
@MyBatisDao
public interface PlanDao extends CrudDao<Plan> {
	// 查询运动方案
	List<Plan> findPlanList(Plan plan);
	// 查询运动方案详情
	Map getPlanInfo(@Param("detailId") String detailId);

}