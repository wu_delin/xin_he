package com.jeesite.modules.recovery.plan.entity;

import javax.validation.constraints.Size;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.recovery.member.entity.RaMember;
import com.jeesite.modules.recovery.therapist.entity.RaTherapist;

/**
 * 方案管理Entity
 * @author qu
 * @version 2025-02-21
 */
@Table(name="ra_plan", alias="a", label="方案管理信息", columns={
		@Column(name="plan_id", attrName="planId", label="方案id", isPK=true),
		@Column(name="apply_id", attrName="applyId", label="预约id"),
		@Column(name="member_id", attrName="memberId", label="用户id"),
		@Column(name="therapist_id", attrName="therapistId", label="治疗师id"),
		@Column(name="plan_title", attrName="planTitle", label="方案标题", queryType=QueryType.LIKE),
		@Column(name="plan_content", attrName="planContent", label="方案内容"),
		@Column(name="start_time", attrName="startTime", label="开始时间", isUpdateForce=true),
		@Column(name="end_time", attrName="endTime", label="结束时间", isUpdateForce=true),
		@Column(includeEntity=DataEntity.class),
	},joinTable={
		@JoinTable(type= JoinTable.Type.LEFT_JOIN, entity= RaTherapist.class, attrName="this", alias="t",
				on="t.therapist_id = a.therapist_id", columns={
				@Column(name="therapist_name", attrName="therapistName", label="治疗师姓名", queryType= QueryType.LIKE),
		}),
		@JoinTable(type= JoinTable.Type.LEFT_JOIN, entity= RaMember.class, attrName="this", alias="b",
				on="b.member_id = a.member_id", columns={
				@Column(name="member_name", attrName="memberName", label="姓名", queryType=QueryType.LIKE),
		}),
}, orderBy="a.update_date DESC"
)
public class Plan extends DataEntity<Plan> {
	
	private static final long serialVersionUID = 1L;
	private String planId;		// 方案id
	private String applyId;		// 预约id
	private String memberId;		// 用户id
	private String memberName;		// 用户姓名
	private String therapistId;		// 治疗师id
	private String therapistName;		// 治疗师姓名
	private String planTitle;		// 方案标题
	private String planContent;		// 方案内容
	private String queryTime;		// 查询时间
	private Date startTime;		// 开始时间
	private Date endTime;		// 结束时间

	public Plan() {
		this(null);
	}
	
	public Plan(String id){
		super(id);
	}
	
	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}
	
	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
	
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public String getTherapistId() {
		return therapistId;
	}

	public void setTherapistId(String therapistId) {
		this.therapistId = therapistId;
	}
	
	@Size(min=0, max=64, message="方案标题长度不能超过 64 个字符")
	public String getPlanTitle() {
		return planTitle;
	}

	public void setPlanTitle(String planTitle) {
		this.planTitle = planTitle;
	}
	
	@Size(min=0, max=500, message="方案内容长度不能超过 500 个字符")
	public String getPlanContent() {
		return planContent;
	}

	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getTherapistName() {
		return therapistName;
	}

	public void setTherapistName(String therapistName) {
		this.therapistName = therapistName;
	}

	public String getQueryTime() {
		return queryTime;
	}

	public void setQueryTime(String queryTime) {
		this.queryTime = queryTime;
	}
}