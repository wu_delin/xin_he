package com.jeesite.modules.recovery.question.entity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

import javax.validation.constraints.Size;

/**
 * 问卷题目Entity
 * @author Mr Wu
 * @version 2024-09-26
 */
@Table(name="ra_ques_option", alias="a", label="问卷题目信息", columns={
		@Column(name="option_id", attrName="optionId", label="选项Id", isPK=true),
		@Column(name="topic_id", attrName="topicId.topicId", label="题目id"),
		@Column(name="option_number", attrName="optionNumber", label="选项编号"),
		@Column(name="option_name", attrName="optionName", label="选项名称", queryType=QueryType.LIKE),
		@Column(name="option_unit", attrName="optionUnit", label="选项单位"),
		@Column(name="option_type", attrName="optionType", label="选项类型", comment="选项类型（0输入，1单选，2多选，3简答）"),
		@Column(name="input_type", attrName="inputType", label="输入类型"),
		@Column(name="input_len", attrName="inputLen", label="输入长度"),
		@Column(name="related_item", attrName="relatedItem", label="关联项目"),
        @Column(name="option_score", attrName="optionScore", label="选项分值"),
	}, orderBy="a.option_id ASC"
)
public class QuestionOption extends DataEntity<QuestionOption> {
	
	private static final long serialVersionUID = 1L;
	private String optionId;		// 选项Id
	private String topicId2;		// 题目id
	private QuestionTopic topicId;	// 题目id 父类
	private String optionNumber;	// 选项编号
	private String optionName;		// 选项名称
	private String optionUnit;		// 选项单位
	private String optionType;		// 选项类型（0输入，1单选，2多选，3简答）
	private String optionScore;		// 选项分值
	private String inputType;		// 输入类型
	private String inputLen;		// 输入长度
	private String relatedItem;		// 关联项目

	public QuestionOption() {
		this(null);
	}

	public QuestionOption(QuestionTopic topicId){
		this.topicId = topicId;
	}

    public String getOptionUnit() {
        return optionUnit;
    }

    public void setOptionUnit(String optionUnit) {
        this.optionUnit = optionUnit;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getInputLen() {
        return inputLen;
    }

    public void setInputLen(String inputLen) {
        this.inputLen = inputLen;
    }

    public String getRelatedItem() {
        return relatedItem;
    }

    public void setRelatedItem(String relatedItem) {
        this.relatedItem = relatedItem;
    }

    public String getTopicId2() {
        return topicId2;
    }

    public void setTopicId2(String topicId2) {
        this.topicId2 = topicId2;
    }

    public String getOptionId() {
		return optionId;
	}

	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}
	
	public QuestionTopic getTopicId() {
		return topicId;
	}

	public void setTopicId(QuestionTopic topicId) {
		this.topicId = topicId;
	}

	public String getOptionNumber() {
		return optionNumber;
	}

	public void setOptionNumber(String optionNumber) {
		this.optionNumber = optionNumber;
	}
	
	@Size(min=0, max=256, message="选项名称长度不能超过 256 个字符")
	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getOptionScore() {
		return optionScore;
	}

	public void setOptionScore(String optionScore) {
		this.optionScore = optionScore;
	}
	
}