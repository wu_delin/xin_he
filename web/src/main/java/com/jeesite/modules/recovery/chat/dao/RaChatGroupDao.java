package com.jeesite.modules.recovery.chat.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.chat.entity.RaChatGroup;

/**
 * 咨询群组DAO接口
 * @author Mr Wu
 * @version 2025-02-20
 */
@MyBatisDao
public interface RaChatGroupDao extends CrudDao<RaChatGroup> {
    // 查询群
    String getMemberGroup(RaChatGroup raChatGroup);
}