package com.jeesite.modules.recovery.member.entity;

import javax.validation.constraints.Size;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 康复预约 用户表Entity
 * @author Mr Wu
 * @version 2025-02-17
 */
@Table(name="ra_member", alias="a", label="康复预约 用户表信息", columns={
		@Column(name="member_id", attrName="memberId", label="id", isPK=true),
		@Column(name="member_name", attrName="memberName", label="姓名", queryType=QueryType.LIKE),
		@Column(name="gender", attrName="gender", label="性别"),
		@Column(name="birthday", attrName="birthday", label="生日"),
		@Column(name="phone", attrName="phone", label="电话", queryType=QueryType.LIKE),
		@Column(name="height", attrName="height", label="身高", isUpdateForce=true),
		@Column(name="weight", attrName="weight", label="体重", isUpdateForce=true),
		@Column(name="openid", attrName="openid", label="微信唯一标识"),
		@Column(name="create_date", attrName="createDate", label="申请时间", isUpdate=false, isQuery=false, isUpdateForce=true),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false, isUpdateForce=true)
}, orderBy="a.update_date DESC"
)
public class RaMember extends DataEntity<RaMember> {

	private static final long serialVersionUID = 1L;
	private String memberId;		// id
	private String memberName;		// 姓名
	private String gender;		// 性别
	private String birthday;		// 生日
	private String phone;		// 电话
	private Double height;		// 身高
	private Double weight;		// 体重
	private String openid;		// 微信唯一标识
	private String role;		// 角色
	private String therapistType;
	private String wxCode;

	public RaMember() {
		this(null);
	}

	public RaMember(String id){
		super(id);
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	@Size(min=0, max=255, message="姓名长度不能超过 255 个字符")
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	@Size(min=0, max=255, message="性别长度不能超过 255 个字符")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Size(min=0, max=255, message="生日长度不能超过 255 个字符")
	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	@Size(min=0, max=255, message="电话长度不能超过 255 个字符")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	@Size(min=0, max=255, message="微信唯一标识长度不能超过 255 个字符")
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getTherapistType() {
		return therapistType;
	}

	public void setTherapistType(String therapistType) {
		this.therapistType = therapistType;
	}

	public String getWxCode() {
		return wxCode;
	}

	public void setWxCode(String wxCode) {
		this.wxCode = wxCode;
	}
}