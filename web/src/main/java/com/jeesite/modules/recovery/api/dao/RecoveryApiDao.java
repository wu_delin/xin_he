package com.jeesite.modules.recovery.api.dao;

import com.jeesite.common.mybatis.annotation.MyBatisDao;
import java.util.List;
import java.util.Map;


/**
 * 公共DAO 接口
 * @author Mr Wu
 * @version 2025-02-17
 */
@MyBatisDao
public interface RecoveryApiDao{
    List<Map> getHomeData(Map map);
}