package com.jeesite.modules.recovery.therapist.service;

import java.util.List;

import com.jeesite.modules.recovery.therapist.entity.RaTherapist;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.therapist.dao.RaTherapistDao;

/**
 * 治疗师列表Service
 * @author Mr Wu
 * @version 2025-02-17
 */
@Service
public class RaTherapistService extends CrudService<RaTherapistDao, RaTherapist> {
	
	/**
	 * 获取单条数据
	 * @param raTherapist
	 * @return
	 */
	@Override
	public RaTherapist get(RaTherapist raTherapist) {
		return super.get(raTherapist);
	}
	
	/**
	 * 查询分页数据
	 * @param raTherapist 查询条件
	 * @param raTherapist page 分页对象
	 * @return
	 */
	@Override
	public Page<RaTherapist> findPage(RaTherapist raTherapist) {
		return super.findPage(raTherapist);
	}
	
	/**
	 * 查询列表数据
	 * @param raTherapist
	 * @return
	 */
	@Override
	public List<RaTherapist> findList(RaTherapist raTherapist) {
		return super.findList(raTherapist);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param raTherapist
	 */
	@Override
	@Transactional
	public void save(RaTherapist raTherapist) {
		super.save(raTherapist);
	}
	
	/**
	 * 删除数据
	 * @param raTherapist
	 */
	@Override
	@Transactional
	public void delete(RaTherapist raTherapist) {
		super.delete(raTherapist);
	}
	
}