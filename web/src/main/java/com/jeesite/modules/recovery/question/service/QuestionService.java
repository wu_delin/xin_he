package com.jeesite.modules.recovery.question.service;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.entity.Page;
import com.jeesite.common.idgen.IdGen;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.question.dao.QuesDao;
import com.jeesite.modules.recovery.question.dao.QuestionTopicDao;
import com.jeesite.modules.recovery.question.entity.Ques;
import com.jeesite.modules.recovery.question.entity.Question;
import com.jeesite.modules.recovery.question.dao.QuestionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 问卷管理Service
 * @author qu
 * @version 2024-09-10
 */
@Service
public class QuestionService extends CrudService<QuestionDao, Question> {

	@Autowired
	private QuesDao quesDao;

	/**
	 * 获取单条数据
	 * @param question
	 * @return
	 */
	@Override
	public Question get(Question question) {
		return super.get(question);
	}
	
	/**
	 * 查询分页数据
	 * @param question 查询条件
	 * @param question page 分页对象
	 * @return
	 */
	@Override
	public Page<Question> findPage(Question question) {
		return super.findPage(question);
	}

	/**
	 * 查询问卷记录数据
	 */
	public Page<Map> findRecordList(Page<Map> page,Map map) {
		map.put("page",page);
		page.setList(dao.findRecordList(map));
		return page;
	}

	/**
	 * 康复师查询总问卷记录
	 */
	public List<Map> findQuesTotalList(String memberCode) {
		return quesDao.findQuesTotalList(memberCode);
	}

	/**
	 * 查询单条问卷记录数据
	 */
	public Map getRecordDetail(Map map) {
		List<Map> recordList = dao.findRecordList(map);
		return recordList.size()>0?recordList.get(0):null;
	}

	/**
	 * 更新问卷信息
	 */
	public void updateQuestionRecord(Map map) {
		dao.updateQuestionRecord(map);
	}
	
	/**
	 * 查询列表数据
	 * @param question
	 * @return
	 */
	@Override
	public List<Question> findList(Question question) {
		return super.findList(question);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param question
	 */
	@Override
	@Transactional
	public void save(Question question) {
		super.save(question);
	}
	
	/**
	 * 更新状态
	 * @param question
	 */
	@Override
	@Transactional
	public void updateStatus(Question question) {
		super.updateStatus(question);
	}
	
	/**
	 * 删除数据
	 * @param question
	 */
	@Override
	@Transactional
	public void delete(Question question) {
		super.delete(question);
	}


	/**
	 * 获取问卷详情
	 * @param questId
	 */
	public Map findQuesDetail(String questId,String memberCode){
		List<Map> question = quesDao.getQuesTitle(questId); // 获取问卷名称
		List<Map> list = quesDao.findQuesTopicList(questId); // 获取问卷题目列表
		Map map = new HashMap();
		map.put("questId",questId);
		map.put("memberCode",memberCode);
		List<Map> optionList = quesDao.findQuesOptionList(map); // 获取问卷选项列表
		Map<String, List<Map>> mapList = optionList.stream().collect(Collectors.groupingBy(item->item.get("topicId").toString()));
		for (Map topic : list) {
			topic.put("optionList",mapList.get(topic.get("topicId")));
		}
		Map mapData = new HashMap();
		mapData.put("question",question);
		mapData.put("questList",list);
		return mapData;
	}

	/**
	 * 保存问卷评估结果
	 */
	@Transactional(readOnly=false)
	public JSONObject saveQuesResult(List<Ques> paramList){
		JSONObject infoMsg = new JSONObject();
		infoMsg.put("result", false);
		infoMsg.put("message","服务异常，请稍后再试！");
		infoMsg.put("data", "");
		try {
			//根据问卷得分获取评估结果
			List<Map> adviceList = quesDao.getQuesResult(paramList);
			Map<String, List<Map>> mapList = adviceList.stream().collect(Collectors.groupingBy(item->item.get("quesId").toString()));
			String scores = "";
			for (Ques ques : paramList) {
				String nextId = IdGen.nextId();
				ques.setScoreId(nextId);
				scores =nextId+","+scores;
				List<Map> rowList = mapList.get(ques.getQuestId());
				if(!Objects.isNull(rowList) && !rowList.isEmpty()){
					if(!"34".equals(ques.getQuestId())){
						ques.setResultInfo(rowList.get(0).get("content").toString());
					}
					ques.setHealthAdvice(rowList.get(0).get("adviceInfo").toString());
				}
			}
			quesDao.insertBatch(paramList);   //保存问卷得分结果
			infoMsg.put("result",true);
			infoMsg.put("message","提交成功！");
			infoMsg.put("data",scores);
			return infoMsg;
		} catch (Exception e) {
			infoMsg.put("message","保存问卷结果出现错误！");
			e.printStackTrace();
		}
		return infoMsg;
	}
	/**
	 * 获取评估问卷记录以及评估详情
	 * */
	public List<Map> findQuesRecordList(Map map){
		final List<Map> list = quesDao.findQuesRecordList(map);
		for(int i=0;i<list.size();i++){
			String ques = list.get(i).get("questId").toString();
			if(("30".equals(ques) || "31".equals(ques))){
				String[] str = {"体质特征","形成原因","发病倾向","保健原则","起居养生","饮食调养","药物调理","精神疗养","","","","",""};
				String[] attr = (list.get(i).get("healthAdvise").toString()).split("##");
				if(attr.length>0) {
					JSONArray array = new JSONArray();
					for (int k = 0; k < attr.length; k++) {
						if (!"\r\n".equals(attr[k])) {
							JSONObject object = new JSONObject();
							object.put("title", str[k]);
							object.put("describe", attr[k]);
							array.add(object);
						}
					}
					list.get(i).put("sign", "0");
					list.get(i).replace("healthAdvise", array);
				}
			}
			if("34".equals(ques)){
				list.get(i).put("sign", "1");
			}
		}
		return list;
	}

	/**
	 * 获取评估问卷题目及问卷费用情况
	 */
	public List<Map> findQuesTitleList(Map<String,String> map) {
		List<Map> list = quesDao.findQuesTitleList(map);
		return list;
	}
}