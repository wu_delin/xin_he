package com.jeesite.modules.recovery.question.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.question.entity.QuestionTopic;

/**
 * 问卷题目DAO接口
 * @author Mr Wu
 * @version 2024-09-26
 */
@MyBatisDao
public interface QuestionTopicDao extends CrudDao<QuestionTopic> {
	
}