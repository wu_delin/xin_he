package com.jeesite.modules.recovery.schedule.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.schedule.entity.RaSchedule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 预约排班表DAO接口
 * @author Mr Wu
 * @version 2025-02-17
 */
@MyBatisDao
public interface RaScheduleDao extends CrudDao<RaSchedule> {
    // 查询可预约日期
    List<RaSchedule> findDateList(RaSchedule raSchedule);
	long delete(RaSchedule raSchedule);
	// 接触与预约申请得绑定
    void cancelBind(@Param("applyId") String applyId);
}