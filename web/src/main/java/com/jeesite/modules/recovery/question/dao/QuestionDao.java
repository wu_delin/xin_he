package com.jeesite.modules.recovery.question.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.question.entity.Question;
import com.jeesite.modules.recovery.question.entity.QuestionTopic;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 问卷管理DAO接口
 * @author qu
 * @version 2024-09-10
 */
@MyBatisDao
public interface QuestionDao extends CrudDao<Question> {
    // 查询问卷记录数据
    List<Map> findRecordList(Map map);
    // 更新问卷信息
	int updateQuestionRecord(Map map);
    // 查询可导出的评估列表
    List<Map> findExportQuesList(Map map);

}