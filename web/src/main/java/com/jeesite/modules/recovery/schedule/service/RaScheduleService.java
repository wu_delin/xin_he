package com.jeesite.modules.recovery.schedule.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.schedule.entity.RaSchedule;
import com.jeesite.modules.recovery.schedule.dao.RaScheduleDao;

/**
 * 预约排班表Service
 * @author Mr Wu
 * @version 2025-02-17
 */
@Service
public class RaScheduleService extends CrudService<RaScheduleDao, RaSchedule> {
	
	/**
	 * 获取单条数据
	 * @param raSchedule
	 * @return
	 */
	@Override
	public RaSchedule get(RaSchedule raSchedule) {
		return super.get(raSchedule);
	}
	
	/**
	 * 查询分页数据
	 * @param raSchedule 查询条件
	 * @param raSchedule page 分页对象
	 * @return
	 */
	@Override
	public Page<RaSchedule> findPage(RaSchedule raSchedule) {
		return super.findPage(raSchedule);
	}
	
	/**
	 * 查询列表数据
	 * @param raSchedule
	 * @return
	 */
	@Override
	public List<RaSchedule> findList(RaSchedule raSchedule) {
		return super.findList(raSchedule);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param raSchedule
	 */
	@Override
	@Transactional
	public void save(RaSchedule raSchedule) {
		super.save(raSchedule);
	}
    @Transactional
    public void saveBatch(List<RaSchedule> list) {
        dao.insertBatch(list);
    }
	
	/**
	 * 删除数据
	 * @param raSchedule
	 */
	@Override
	@Transactional
	public void delete(RaSchedule raSchedule) {
		dao.delete(raSchedule);
	}

	/**
	 * 查询可预约日期
	 * @param raSchedule
	 */
	public JSONObject findDateList(RaSchedule raSchedule){
		JSONObject object = new JSONObject();
		List<RaSchedule> list = dao.findDateList(raSchedule);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, List<RaSchedule>> listData = list.stream()
				.collect(Collectors.groupingBy(
						ra -> dateFormat.format(ra.getScheduleDate())
				));
		object.put("data", listData);
		return object;
	}
}