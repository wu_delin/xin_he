package com.jeesite.modules.recovery.question.entity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

import javax.validation.constraints.Size;

/**
 * 问卷管理 Entity
 * @author qu
 * @version 2024-09-10
 */
@Table(name="ra_ques_title", alias="a", label="问卷管理信息", columns={
		@Column(name="quest_id", attrName="questId", label="调查问卷ID", isPK=true),
		@Column(name="quest_name", attrName="questName", label="调查问卷名称", queryType=QueryType.LIKE),
		@Column(name="quest_type", attrName="questType", label="问卷分类", comment="问卷分类（0健康  1心理）"),
		@Column(name="apply_sex", attrName="applySex", label="适用性别", comment="适用性别（0通用，1男，2女）"),
		@Column(name="quest_score", attrName="questScore", label="问卷总分"),
		@Column(name="quest_time", attrName="questTime", label="问卷时长", comment="问卷时长(以分为单位，0代表不限时)", isUpdateForce=true),
		@Column(name="money", attrName="money", label="问卷费用", comment="问卷费用(以分为单位，0代表不要钱)", isUpdateForce=true),
		@Column(name="calc_rule", attrName="calcRule", label="计算公式"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class Question extends DataEntity<Question> {
	
	private static final long serialVersionUID = 1L;
	private String questId;		// 调查问卷ID
	private String questName;	// 调查问卷名称
	private String questType;	// 问卷分类（0健康  1心理）
	private String applySex;	// 适用性别（0通用，1男，2女）
	private String questClass;	// 问卷类型0：小程序健康评估1：系统诊疗随访
	private String questScore;	// 问卷总分
	private Long questTime;		// 问卷时长(以分为单位，0代表不限时)
	private Long money;		    // 问卷费用(以分为单位，0代表不要钱)
	private String calcRule;	// 计算公式

	public Question() {
		this(null);
	}
	
	public Question(String id){
		super(id);
	}
	
	public String getQuestId() {
		return questId;
	}

	public void setQuestId(String questId) {
		this.questId = questId;
	}
	
	@Size(min=0, max=100, message="调查问卷名称长度不能超过 100 个字符")
	public String getQuestName() {
		return questName;
	}

	public void setQuestName(String questName) {
		this.questName = questName;
	}
	
	@Size(min=0, max=1, message="问卷分类长度不能超过 1 个字符")
	public String getQuestType() {
		return questType;
	}

	public void setQuestType(String questType) {
		this.questType = questType;
	}

	public String getApplySex() {
		return applySex;
	}

	public void setApplySex(String applySex) {
		this.applySex = applySex;
	}
	
	@Size(min=0, max=1, message="问卷类型0长度不能超过 1 个字符")
	public String getQuestClass() {
		return questClass;
	}

	public void setQuestClass(String questClass) {
		this.questClass = questClass;
	}
	
	@Size(min=0, max=10, message="问卷总分长度不能超过 10 个字符")
	public String getQuestScore() {
		return questScore;
	}

	public void setQuestScore(String questScore) {
		this.questScore = questScore;
	}
	
	public Long getQuestTime() {
		return questTime;
	}

	public void setQuestTime(Long questTime) {
		this.questTime = questTime;
	}
	
	public Long getMoney() {
		return money;
	}

	public void setMoney(Long money) {
		this.money = money;
	}
	
	public String getCalcRule() {
		return calcRule;
	}

	public void setCalcRule(String calcRule) {
		this.calcRule = calcRule;
	}
	
}