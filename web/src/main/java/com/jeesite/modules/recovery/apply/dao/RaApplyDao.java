package com.jeesite.modules.recovery.apply.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.apply.entity.RaApply;
import org.apache.ibatis.annotations.Param;

import java.util.Map;


/**
 * 申请 康复治疗预约DAO接口
 * @author Mr Wu
 * @version 2025-02-17
 */
@MyBatisDao
public interface RaApplyDao extends CrudDao<RaApply> {
	// 康复师查询处理数据
	Map getApplyInfo(@Param("applyId") String applyId);
}