package com.jeesite.modules.recovery.api.service;

import com.jeesite.common.entity.Page;
import com.jeesite.modules.recovery.api.dao.RecoveryApiDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * 公共 Service
 * @author Mr Wu
 * @version 2025-02-25
 */
@Service
public class RecoveryApiService{

    @Autowired
    private RecoveryApiDao dao;

	/**
	 * 查询列表数据
	 * @return
	 */
	public Page<Map> getHomeData(Page page, Map map) {
        map.put("page",page);
        page.setList(dao.getHomeData(map));
        return page;
	}

}