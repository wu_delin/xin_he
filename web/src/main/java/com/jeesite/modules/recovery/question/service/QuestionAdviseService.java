package com.jeesite.modules.recovery.question.service;

import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.question.dao.QuestionAdviseDao;
import com.jeesite.modules.recovery.question.entity.QuestionAdvise;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 问卷建议表Service
 * @author Mr Wu
 * @version 2024-10-16
 */
@Service
public class QuestionAdviseService extends CrudService<QuestionAdviseDao, QuestionAdvise> {

	/**
	 * 查询列表数据
	 * @param questionAdvise
	 * @return
	 */
	@Override
	public List<QuestionAdvise> findList(QuestionAdvise questionAdvise) {
		return super.findList(questionAdvise);
	}
	
	/**
	 * 保存数据
	 * @param questionAdvise
	 */
	@Transactional
	public void saveData(QuestionAdvise questionAdvise) {
        List<QuestionAdvise> list = questionAdvise.getDataList();
        List<QuestionAdvise> add = new ArrayList<>();
        List<QuestionAdvise> update = new ArrayList<>();
        List<QuestionAdvise> del = new ArrayList<>();
        for (QuestionAdvise advise : list) {
            if("".equals(advise.getStatus())){
                // 修改
                update.add(advise);
            }
            if("0".equals(advise.getStatus())){
                // 新增
                advise.setQuestId(questionAdvise.getQuestId());
                add.add(advise);
            }
            if("1".equals(advise.getStatus())){
                // 删除
                del.add(advise);
            }
        }
        if(add.size()>0 || update.size()>0 || del.size()>0){
            Map map = new HashMap();
            map.put("addList",add);
            map.put("delList",del);
            map.put("updateList",update);
            dao.saveData(map);
        }
	}

	
}