package com.jeesite.modules.recovery.plan.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.plan.entity.Plan;
import com.jeesite.modules.recovery.plan.dao.PlanDao;

/**
 * 方案管理Service
 * @author qu
 * @version 2025-02-21
 */
@Service
public class PlanService extends CrudService<PlanDao, Plan> {
	
	/**
	 * 获取单条数据
	 * @param plan
	 * @return
	 */
	@Override
	public Plan get(Plan plan) {
		return super.get(plan);
	}
	
	/**
	 * 查询分页数据
	 * @param plan 查询条件
	 * @param plan page 分页对象
	 * @return
	 */
	@Override
	public Page<Plan> findPage(Plan plan) {
		return super.findPage(plan);
	}
	
	/**
	 * 查询列表数据
	 * @param plan
	 * @return
	 */
	@Override
	public List<Plan> findList(Plan plan) {
		return super.findList(plan);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param plan
	 */
	@Override
	@Transactional
	public void save(Plan plan) {
		super.save(plan);
	}
	
	/**
	 * 更新状态
	 * @param plan
	 */
	@Override
	@Transactional
	public void updateStatus(Plan plan) {
		super.updateStatus(plan);
	}
	
	/**
	 * 删除数据
	 * @param plan
	 */
	@Override
	@Transactional
	public void delete(Plan plan) {
		super.delete(plan);
	}

	/**
	 * 查询运动方案
	 * @param plan
	 */
	public Page<Plan> findPlanList(Plan plan) {
		return plan.getPage().setList(dao.findPlanList(plan));
	}

	/**
	 * 查询运动方案详情
	 * @param detailId
	 */
	public Map getPlanInfo(String detailId) {
		return dao.getPlanInfo(detailId);
	}
}