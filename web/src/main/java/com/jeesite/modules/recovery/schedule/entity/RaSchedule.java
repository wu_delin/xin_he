package com.jeesite.modules.recovery.schedule.entity;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.recovery.therapist.entity.RaTherapist;
import com.jeesite.modules.sys.entity.Office;
import com.jeesite.modules.sys.entity.User;

import java.util.Date;

/**
 * 预约排班表Entity
 * @author Mr Wu
 * @version 2025-02-17
 */
@Table(name="ra_schedule", alias="a", label="预约排班表信息", columns={
		@Column(name="schedule_id", attrName="scheduleId", label="排班计划id", isPK=true),
		@Column(name="schedule_date", attrName="scheduleDate", label="排班日期"),
		@Column(name="time_period", attrName="timePeriod", label="时间段"),
		@Column(name="apply_id", attrName="applyId", label="绑定的申请id "),
		@Column(name="therapist_id", attrName="therapistId", label="康复师id"),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
	},joinTable={
        @JoinTable(type= JoinTable.Type.LEFT_JOIN, entity= RaTherapist.class, attrName="this", alias="t",
                on="t.therapist_id = a.therapist_id", columns={
                @Column(name="therapist_name", attrName="therapistName", label="治疗师姓名", queryType=QueryType.LIKE),
        }),
    }, orderBy="a.update_date DESC"
)
public class RaSchedule extends DataEntity<RaSchedule> {
	
	private static final long serialVersionUID = 1L;
	private String scheduleId;		// 排班计划id
	private Date scheduleDate;		// 排班日期
	private String timePeriod;		// 时间段
	private String therapistId;		// 康复师id
	private String therapistName;	// 康复师
	//	接口变量
	private String startDate;		// 开始日期
	private String endDate;		// 结束日期
	private String applyId; // 申请id
	private String therapistSex; // 康复师性别
	private String therapistType;  // 康复师类型
	private String therapistPhone;  // 康复师电话

	public RaSchedule() {
		this(null);
	}
	
	public RaSchedule(String id){
		super(id);
	}
	
	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

    @JsonFormat(pattern = "yyyy-MM-dd")
	public Date getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public String getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}
	
	@Size(min=0, max=64, message="康复师id长度不能超过 64 个字符")
	public String getTherapistId() {
		return therapistId;
	}

	public void setTherapistId(String therapistId) {
		this.therapistId = therapistId;
	}

    public String getTherapistName() {
        return therapistName;
    }

    public void setTherapistName(String therapistName) {
        this.therapistName = therapistName;
    }

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTherapistSex() {
		return therapistSex;
	}

	public void setTherapistSex(String therapistSex) {
		this.therapistSex = therapistSex;
	}

	public String getTherapistType() {
		return therapistType;
	}

	public void setTherapistType(String therapistType) {
		this.therapistType = therapistType;
	}

	public String getTherapistPhone() {
		return therapistPhone;
	}

	public void setTherapistPhone(String therapistPhone) {
		this.therapistPhone = therapistPhone;
	}

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
}