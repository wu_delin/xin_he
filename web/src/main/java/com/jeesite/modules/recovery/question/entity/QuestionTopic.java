package com.jeesite.modules.recovery.question.entity;

import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 问卷题目Entity
 * @author Mr Wu
 * @version 2024-09-26
 */
@Table(name="ra_ques_topic", alias="a", label="问卷题目信息", columns={
		@Column(name="topic_id", attrName="topicId", label="题目id", isPK=true),
		@Column(name="quest_id", attrName="questId", label="问卷id"),
		@Column(name="topic_number", attrName="topicNumber", label="题目编号",isQuery = false),
		@Column(name="topic_content", attrName="topicContent", label="题目内容", queryType=QueryType.LIKE),
		@Column(name="topic_type", attrName="topicType", label="题目类型", comment="题目类型（0单选，1输入）"),
		@Column(name="topic_rule", attrName="topicRule", label="题目解析规则"),
		@Column(name="status", attrName="status", label="状态", comment="状态（0正常 1删除 2停用）", isUpdate=false),
	}, orderBy="a.topic_number ASC"
)
public class QuestionTopic extends DataEntity<QuestionTopic> {
	
	private static final long serialVersionUID = 1L;
	private String topicId;		// 题目id
	private String questId;		// 问卷id
	private long topicNumber;	// 题目编号
	private String topicContent;// 题目内容
	private String topicType;	// 题目类型（0单选，1输入）
	private String topicRule;	// 题目解析规则
	private List<QuestionOption> questionOptionList = ListUtils.newArrayList();		// 子表列表

	public QuestionTopic() {
		this(null);
	}
	
	public QuestionTopic(String id){
		super(id);
	}
	
	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	
	@NotBlank(message="问卷id不能为空")
	@Size(min=0, max=64, message="问卷id长度不能超过 64 个字符")
	public String getQuestId() {
		return questId;
	}

	public void setQuestId(String questId) {
		this.questId = questId;
	}

	public long getTopicNumber() {
		return topicNumber;
	}

	public void setTopicNumber(long topicNumber) {
		this.topicNumber = topicNumber;
	}
	
	@NotBlank(message="题目内容不能为空")
	@Size(min=0, max=256, message="题目内容长度不能超过 256 个字符")
	public String getTopicContent() {
		return topicContent;
	}

	public void setTopicContent(String topicContent) {
		this.topicContent = topicContent;
	}

	public String getTopicType() {
		return topicType;
	}

	public void setTopicType(String topicType) {
		this.topicType = topicType;
	}
	
	public String getTopicRule() {
		return topicRule;
	}

	public void setTopicRule(String topicRule) {
		this.topicRule = topicRule;
	}
	
	@Valid
	public List<QuestionOption> getQuestionOptionList() {
		return questionOptionList;
	}

	public void setQuestionOptionList(List<QuestionOption> questionOptionList) {
		this.questionOptionList = questionOptionList;
	}
	
}