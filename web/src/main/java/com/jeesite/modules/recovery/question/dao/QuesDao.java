package com.jeesite.modules.recovery.question.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.question.entity.Ques;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 问卷管理 DAO接口
 * @author jeffery
 * @version 2024-10-14
 */
@MyBatisDao
public interface QuesDao extends CrudDao<Ques> {
	// 获取评估问卷题目列表
	List<Map> findQuesTitleList(Map map);

	// 获取问卷名称
	List<Map> getQuesTitle(@Param("questId") String questId);

	// 获取问卷题目列表
	List<Map> findQuesTopicList(@Param("questId") String questId);

	// 获取问卷选项列表
	List<Map> findQuesOptionList(Map map);

	// 根据问卷得分获取评估结果
	List<Map> getQuesResult(List<Ques> paramList);

	// 获取评估问卷记录以及评估详情
	List<Map> findQuesRecordList(Map map);

	// 康复师查询总问卷记录
	List<Map> findQuesTotalList(@Param("memberCode") String memberCode);
}