package com.jeesite.modules.recovery.therapist.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.therapist.entity.RaTherapist;

/**
 * 治疗师列表DAO接口
 * @author Mr Wu
 * @version 2025-02-17
 */
@MyBatisDao
public interface RaTherapistDao extends CrudDao<RaTherapist> {
	
}