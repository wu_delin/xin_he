package com.jeesite.modules.recovery.question.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.question.entity.QuestionAdvise;

import java.util.Map;

/**
 * 问卷建议表DAO接口
 * @author Mr Wu
 * @version 2024-10-16
 */
@MyBatisDao
public interface QuestionAdviseDao extends CrudDao<QuestionAdvise> {
	void saveData(Map map);
}