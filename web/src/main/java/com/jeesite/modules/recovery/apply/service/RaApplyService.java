package com.jeesite.modules.recovery.apply.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.apply.dao.RaApplyDao;
import com.jeesite.modules.recovery.apply.entity.RaApply;
import com.jeesite.modules.recovery.schedule.dao.RaScheduleDao;
import com.jeesite.modules.recovery.schedule.entity.RaSchedule;
import com.jeesite.modules.recovery.schedule.service.RaScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 申请 康复治疗预约Service
 * @author Mr Wu
 * @version 2025-02-17
 */
@Service
public class RaApplyService extends CrudService<RaApplyDao, RaApply> {

    @Autowired
    private RaScheduleDao raScheduleDao;

	/**
	 * 获取单条数据
	 * @param raApply
	 * @return
	 */
	@Override
	public RaApply get(RaApply raApply) {
		return super.get(raApply);
	}
	
	/**
	 * 查询分页数据
	 * @param raApply 查询条件
	 * @param raApply page 分页对象
	 * @return
	 */
	@Override
	public Page<RaApply> findPage(RaApply raApply) {
		return super.findPage(raApply);
	}
	
	/**
	 * 查询列表数据
	 * @param raApply
	 * @return
	 */
	@Override
	public List<RaApply> findList(RaApply raApply) {
		return super.findList(raApply);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param raApply
	 */
	@Override
	@Transactional
	public void save(RaApply raApply) {
		super.save(raApply);
	}
	
	/**
	 * 更新申请信息
	 * @param raApply
	 */
	@Transactional(rollbackFor = Exception.class)
	public void updateApplyInfo(RaApply raApply) {
		dao.update(raApply);
		if("1".equals(raApply.getApplyState())){
            raScheduleDao.cancelBind(raApply.getApplyId());
        }
	}
	
	/**
	 * 删除数据
	 * @param raApply
	 */
	@Override
	@Transactional
	public void delete(RaApply raApply) {
		super.delete(raApply);
	}

	/**
	 * 康复师查询处理数据
	 * @param applyId
	 */
	public Map getApplyInfo(String applyId) {
		return dao.getApplyInfo(applyId);
	}
}