package com.jeesite.modules.recovery.api.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.api.service.RecoveryApiService;
import com.jeesite.modules.recovery.apply.entity.RaApply;
import com.jeesite.modules.recovery.apply.service.RaApplyService;
import com.jeesite.modules.recovery.chat.entity.RaChat;
import com.jeesite.modules.recovery.chat.entity.RaChatGroup;
import com.jeesite.modules.recovery.chat.service.RaChatGroupService;
import com.jeesite.modules.recovery.chat.service.RaChatService;
import com.jeesite.modules.recovery.member.entity.RaMember;
import com.jeesite.modules.recovery.member.service.RaMemberService;
import com.jeesite.modules.recovery.plan.entity.Plan;
import com.jeesite.modules.recovery.plan.service.PlanService;
import com.jeesite.modules.recovery.question.entity.Ques;
import com.jeesite.modules.recovery.question.service.QuestionService;
import com.jeesite.modules.recovery.schedule.entity.RaSchedule;
import com.jeesite.modules.recovery.schedule.service.RaScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 康复治疗预约接口 Controller
 * @author Mr Wu
 * @version 2025-02-20
 */
@Controller
@RequestMapping(value = "${adminPath}/recovery/api")
public class RecoveryApiController extends BaseController {

    @Autowired
    private RecoveryApiService recoveryApiService;
    @Autowired
    private RaMemberService raMemberService;
    @Autowired
    private RaApplyService raApplyService;
    @Autowired
    private RaScheduleService raScheduleService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private RaChatGroupService raChatGroupService;
    @Autowired
    private RaChatService raChatService;
    @Autowired
    private PlanService planService;

    // =============== 登录 ===============

    /**
     *  查询登录信息【api】
     * */
    @RequestMapping(value = "getLoginInfo")
    @ResponseBody
    public String getLoginInfo(RaMember raMember) {
        RaMember member = raMemberService.getLoginInfo(raMember);
        if(null==member){
            return renderResult(Global.FALSE, text("登录失败！"));
        }
        return renderResult(Global.TRUE, text("登录成功！"),member);
    }

    /**
     *  刷新登录信息【api】
     * */
    @RequestMapping(value = "refreshLoginInfo")
    @ResponseBody
    public String refreshLoginInfo(RaMember raMember) {
        RaMember member = raMemberService.refreshLoginInfo(raMember);
        return renderResult(Global.TRUE, text("success"),member);
    }

    /**
     *  更新登录信息【api】
     * */
    @RequestMapping(value = "updateLoginInfo")
    @ResponseBody
    public String updateLoginInfo(RaMember raMember) {
        raMemberService.update(raMember);
        return renderResult(Global.TRUE, text("保存成功！"));
    }

    // =============== 治疗师数据 ===============

    /**
     * 患者列表
     * */
    @RequestMapping(value = "getHomeData")
    @ResponseBody
    public Page<Map> getHomeData(HttpServletRequest request, HttpServletResponse response) {
        Map map = new HashMap();
        map.put("therapistId",request.getParameter("therapistId"));
        map.put("applyState",request.getParameter("applyState"));
        Page<Map> page = recoveryApiService.getHomeData(new Page<>(request, response), map);
        return page;
    }

    // =============== 问卷评估 ===============

    /**
     * 问卷评估页面
     */
    @RequestMapping(value = "assessList")
    public String question() {
        return "modules/recovery/question/assessList";
    }

    /**
     * 获取评估问卷记录以及评估详情
     * scoreId不为空时进行问卷详情的查询
     * */
    @RequestMapping(value = "findQuesRecordList")
    @ResponseBody
    public String findQuesRecordList(HttpServletRequest request){
        Map map = new HashMap();
        map.put("memberCode",request.getParameter("memberCode"));
        map.put("questId",request.getParameter("questId"));
        map.put("scoreId",request.getParameter("scoreId"));
        List<Map> list = questionService.findQuesRecordList(map);
        return renderResult(Global.TRUE, text("success"), list);
    }

    /**
     * 获取问卷详情
     */
    @RequestMapping(value = "findQuesDetail")
    @ResponseBody
    public Map findQuesDetail(HttpServletRequest request) {
        String questId = request.getParameter("questId");
        String member = request.getParameter("memberCode");
        Map map = questionService.findQuesDetail(questId,member);
        return map;
    }

    /**
     * 保存问卷评估结果
     */
    @RequestMapping(value = "saveQuesResult")
    @ResponseBody
    public String saveQuesResult(String paramsData) {
        List<Ques> list = JSON.parseArray(paramsData,Ques.class);
        JSONObject result = questionService.saveQuesResult(list);
        return renderResult(result.getString("result"), text(result.getString("message")),result.getString("data"));
    }

    /**
     * 获取评估问卷题目及问卷费用情况
     */
    @RequestMapping(value = "findQuesTitleList")
    @ResponseBody
    public String findQuesTitleList(HttpServletRequest request) {
        Map map = new HashMap();
        map.put("memberCode",request.getParameter("memberCode"));
        map.put("applySex",request.getParameter("applySex"));
        map.put("questType",request.getParameter("questType"));
        List<Map> list =  questionService.findQuesTitleList(map);
        return renderResult(Global.TRUE, text("success"),list);
    }

    /**
     * 查询指定用户的评估记录
     */
    @RequestMapping(value = "findAssessList")
    @ResponseBody
    public Page<Map> getAssessList(HttpServletRequest request, HttpServletResponse response) {
        Map map = new HashMap();
        map.put("memberId",request.getParameter("memberId"));
        Page<Map> page = questionService.findRecordList(new Page<>(request, response),map);
        return page;
    }

    /**
     * 康复师查询总问卷记录
     */
    @RequestMapping(value = "findQuesTotalList")
    @ResponseBody
    public String findQuesTotalList(String memberCode) {
        List<Map> list = questionService.findQuesTotalList(memberCode);
        return renderResult(Global.TRUE, text("success"),list);
    }

    // =============== 排班 ===============

    /**
     * 查询可预约日期
     */
    @RequestMapping(value = "findDateList")
    @ResponseBody
    public String findDateList(RaSchedule raSchedule) {
        JSONObject list = raScheduleService.findDateList(raSchedule);
        return renderResult(Global.TRUE, text("success"),list);
    }

    // =============== 申请预约 ===============

    /**
     * 保存预约数据
     */
    @PostMapping(value = "saveApply")
    @ResponseBody
    public String saveApply(RaApply raApply,RaChatGroup raChatGroup) {
        raApply.setApplyDate(new Date());
        raApplyService.insert(raApply);
        RaSchedule raSchedule = new RaSchedule();
        raSchedule.setScheduleId(raApply.getScheduleId());
        raSchedule.setApplyId(raApply.getApplyId());
        raScheduleService.update(raSchedule);
        raChatService.serviceMemberGroup(raChatGroup); // 创建聊天群
        return renderResult(Global.TRUE, text("预约康复治疗成功！"));
    }

    /**
     * 查询我的就诊记录
     */
    @RequestMapping(value = "findApplyList")
    @ResponseBody
    public Page<RaApply> findApplyList(RaApply raApply, HttpServletRequest request, HttpServletResponse response) {
        raApply.setPage(new Page<>(request, response));
        Page<RaApply> page = raApplyService.findPage(raApply);
        return page;
    }

    /**
     * 更新申请信息
     */
    @RequestMapping(value = "updateApplyInfo")
    @ResponseBody
    public String updateApplyInfo(RaApply raApply) {
        raApplyService.updateApplyInfo(raApply);
        return renderResult(Global.TRUE, text("更新就诊状态成功！"));
    }

    /**
     * 康复师查询数据
     */
    @RequestMapping(value = "getApplyInfo")
    @ResponseBody
    public String getApplyInfo(String applyId) {
        Map map = raApplyService.getApplyInfo(applyId);
        return renderResult(Global.TRUE, text("success！"),map);
    }

    /**
     * 保存治疗数据
     */
    @RequestMapping(value = "saveHealData")
    @ResponseBody
    public String saveHealData(RaApply raApply) {
        raApplyService.update(raApply);
        return renderResult(Global.TRUE, text("保存治疗数据成功！"));
    }

    // =============== 咨询聊天 ===============

    /**
     * 查询未读消息条数
     */
    @RequestMapping(value = "getUnreadTotal")
    @ResponseBody
    public String getUnreadTotal(String senderId) {
        Integer num = raChatService.getUnreadTotal(senderId);
        return renderResult(Global.TRUE, text("success！"), num);
    }

    /**
     * 根据用户id，治疗师id 获取聊天群组
     */
    @RequestMapping(value = "getChatGroup")
    @ResponseBody
    public String getChatGroup(RaChatGroup raChatGroup) {
        List<RaChatGroup> list = raChatGroupService.findList(raChatGroup);
        if (list.size()>0) {
            return renderResult(Global.TRUE, text("获取聊天群组成功！"), list.get(0));
        }else {
            return renderResult(Global.FALSE, text("获取聊天群组失败！"));
        }
    }

    /**
     * 获取自己的聊天群组
     */
    @RequestMapping(value = "findGroupList")
    @ResponseBody
    public Page<RaChat> findGroupList(RaChat raChat, HttpServletRequest request, HttpServletResponse response) {
        raChat.setPage(new Page<>(request, response));
        Page<RaChat> page = raChatService.findGroupList(raChat);
        return page;
    }

    /**
     * 保存聊天数据
     */
    @PostMapping(value = "saveChatData")
    @ResponseBody
    public String saveChatData(RaChat raChat) {
        raChat.setSenderDate(new Date());
        raChatService.insert(raChat);
        return renderResult(Global.TRUE, text("保存成功！"));
    }

    /**
     * 更新为已读
     */
    @PostMapping(value = "updateUnread")
    @ResponseBody
    public void updateUnread(RaChat raChat) {
        raChatService.updateUnread(raChat);
    }

    /**
     * 查询聊天数据
     */
    @RequestMapping(value = "findChatData")
    @ResponseBody
    public Page<RaChat> findChatData(RaChat raChat, HttpServletRequest request, HttpServletResponse response) {
        raChat.setPage(new Page<>(request, response));
        Page<RaChat> page = raChatService.findPage(raChat);
        return page;
    }

    // =============== 运动训练 ===============

    /**
     * 查询运动方案
     */
    @RequestMapping(value = "findPlanList")
    @ResponseBody
    public Page<Plan> findPlanList(Plan plan, HttpServletRequest request, HttpServletResponse response) {
        plan.setPage(new Page<>(request, response));
        Page<Plan> page = planService.findPlanList(plan);
        return page;
    }

    /**
     * 查询运动方案详情
     */
    @PostMapping(value = "getPlanInfo")
    @ResponseBody
    public String getPlanInfo(String detailId) {
        Map map = planService.getPlanInfo(detailId);
        return renderResult(Global.TRUE, text("success"),map);
    }

    /**
     * 保存运动方案
     */
    @PostMapping(value = "insertPlanInfo")
    @ResponseBody
    public String insertPlanInfo(Plan plan) {
        planService.insert(plan);
        return renderResult(Global.TRUE, text("success"));
    }
}