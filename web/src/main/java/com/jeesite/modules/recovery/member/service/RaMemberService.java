package com.jeesite.modules.recovery.member.service;

import java.util.List;
import java.util.Objects;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.web.http.HttpClientUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.member.entity.RaMember;
import com.jeesite.modules.recovery.member.dao.RaMemberDao;

/**
 * 康复预约 用户表Service
 * @author Mr Wu
 * @version 2025-02-17
 */
@Service
public class RaMemberService extends CrudService<RaMemberDao, RaMember> {

	/**
	 * 获取单条数据
	 * @param raMember
	 * @return
	 */
	@Override
	public RaMember get(RaMember raMember) {
		return super.get(raMember);
	}
	
	/**
	 * 查询分页数据
	 * @param raMember 查询条件
	 * @param raMember page 分页对象
	 * @return
	 */
	@Override
	public Page<RaMember> findPage(RaMember raMember) {
		return super.findPage(raMember);
	}
	
	/**
	 * 查询列表数据
	 * @param raMember
	 * @return
	 */
	@Override
	public List<RaMember> findList(RaMember raMember) {
		return super.findList(raMember);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param raMember
	 */
	@Override
	@Transactional
	public void save(RaMember raMember) {
		super.save(raMember);
	}
	
	/**
	 * 删除数据
	 * @param raMember
	 */
	@Override
	@Transactional
	public void delete(RaMember raMember) {
		super.delete(raMember);
	}

	/**
	 *  查询登录信息【api】
	 * */
	public RaMember getLoginInfo(RaMember raMember) {
		String open = getOpenid(raMember.getWxCode());
		if (StringUtils.isEmpty(open)) {
			return null;
		}
		raMember.setOpenid(open);
		List<RaMember> list = dao.getLoginInfo(raMember);
		if(list.size()>0){
			dao.update(list.get(0));
            return list.get(0);
		}else{
			dao.insert(raMember);
            return raMember;
		}
	}

	/**
	 *  刷新登录信息【api】
	 * */
	public RaMember refreshLoginInfo(RaMember raMember) {
		RaMember member = null;
		List<RaMember> list = dao.getLoginInfo(raMember);
		if(list.size()>0){
			member = list.get(0);
		}
		return member;
	}

	/**
	 * 获取微信openid
	 * */
	private String getOpenid(String code){
		String appID = "wx20d1478425ee81bb";
		String appSecret = "4fa2b085c0cc732629ecb337a3bb5eb4";
		try{
			String result = HttpClientUtils.get("https://api.weixin.qq.com/sns/jscode2session?appid="
					+ appID + "&secret="
					+ appSecret + "&js_code="
					+ code
					+ "&grant_type=authorization_code");
			JSONObject jsonObject =  JSON.parseObject(result);
			Object openid = jsonObject.get("openid");
			return StringUtils.isEmpty((CharSequence) openid)?"":openid.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}