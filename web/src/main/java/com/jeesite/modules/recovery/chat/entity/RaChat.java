package com.jeesite.modules.recovery.chat.entity;

import javax.validation.constraints.Size;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 聊天Entity
 * @author Mr Wu
 * @version 2025-02-20
 */
@Table(name="ra_chat", alias="a", label="聊天信息", columns={
		@Column(name="chat_id", attrName="chatId", label="聊天id", isPK=true),
		@Column(name="sender_id", attrName="senderId", label="发送者"),
		@Column(name="group_id", attrName="groupId", label="所属群组"),
		@Column(name="sender_date", attrName="senderDate", label="发送时间", isUpdateForce=true),
		@Column(name="sender_role", attrName="senderRole", label="发送者角色"),
		@Column(name="send_msg", attrName="sendMsg", label="发送的内容"),
		@Column(name="unread", attrName="unread", label="消息状态【0未读1已读】"),
	}, orderBy="a.sender_date ASC"
)
public class RaChat extends DataEntity<RaChat> {
	
	private static final long serialVersionUID = 1L;
	private String chatId;		// 聊天id
	private String senderId;		// 发送者
	private String groupId;		// 所属群组
	private Date senderDate;		// 发送时间
	private String senderRole;		// 发送者角色
	private String sendMsg;		// 发送的内容
	private String unread;		// 消息状态【0未读1已读】
	private String groupName;		// 群名称

	public RaChat() {
		this(null);
	}
	
	public RaChat(String id){
		super(id);
	}
	
	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	
	@Size(min=0, max=64, message="发送者长度不能超过 64 个字符")
	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	
	@Size(min=0, max=64, message="所属群组长度不能超过 64 个字符")
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getSenderDate() {
		return senderDate;
	}

	public void setSenderDate(Date senderDate) {
		this.senderDate = senderDate;
	}
	
	@Size(min=0, max=255, message="发送者角色长度不能超过 255 个字符")
	public String getSenderRole() {
		return senderRole;
	}

	public void setSenderRole(String senderRole) {
		this.senderRole = senderRole;
	}
	
	@Size(min=0, max=255, message="发送的内容长度不能超过 255 个字符")
	public String getSendMsg() {
		return sendMsg;
	}

	public void setSendMsg(String sendMsg) {
		this.sendMsg = sendMsg;
	}
	
	@Size(min=0, max=1, message="消息状态【0未读1已读】长度不能超过 1 个字符")
	public String getUnread() {
		return unread;
	}

	public void setUnread(String unread) {
		this.unread = unread;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}