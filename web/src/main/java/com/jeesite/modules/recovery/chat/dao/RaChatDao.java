package com.jeesite.modules.recovery.chat.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.chat.entity.RaChat;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 聊天DAO接口
 * @author Mr Wu
 * @version 2025-02-20
 */
@MyBatisDao
public interface RaChatDao extends CrudDao<RaChat> {
	// 查询群列表
    List<RaChat> findGroupList(RaChat raChat);
    // 更新为已读
    long updateUnread(RaChat raChat);
    // 查询未读消息条数
    Integer getUnreadTotal(@Param("senderId") String senderId);
}