package com.jeesite.modules.recovery.therapist.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.jeesite.modules.recovery.therapist.entity.RaTherapist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.therapist.service.RaTherapistService;

/**
 * 治疗师列表Controller
 * @author Mr Wu
 * @version 2025-02-17
 */
@Controller
@RequestMapping(value = "${adminPath}/recovery/therapist")
public class RaTherapistController extends BaseController {

	@Autowired
	private RaTherapistService raTherapistService;

	/**
	 * 获取数据
	 */
	@ModelAttribute
	public RaTherapist get(String therapistId, boolean isNewRecord) {
		return raTherapistService.get(therapistId, isNewRecord);
	}

	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(RaTherapist raTherapist, Model model) {
		model.addAttribute("therapist", raTherapist);
		return "modules/recovery/therapist/therapistList";
	}

	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<RaTherapist> listData(RaTherapist raTherapist, HttpServletRequest request, HttpServletResponse response) {
		raTherapist.setPage(new Page<>(request, response));
		Page<RaTherapist> page = raTherapistService.findPage(raTherapist);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(RaTherapist raTherapist, Model model) {
		model.addAttribute("therapist", raTherapist);
		return "modules/recovery/therapist/therapistForm";
	}

	/**
	 * 保存数据
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated RaTherapist raTherapist) {
		raTherapistService.save(raTherapist);
		return renderResult(Global.TRUE, text("保存治疗师列表成功！"));
	}

	/**
	 * 删除数据
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(RaTherapist raTherapist) {
		raTherapistService.delete(raTherapist);
		return renderResult(Global.TRUE, text("删除治疗师列表成功！"));
	}

	/**
	 * 治疗师选择
	 */
	@RequestMapping(value = "therapistSelect")
	public String therapistSelect(RaTherapist raTherapist, Model model) {
		model.addAttribute("therapist", raTherapist);
		return "modules/recovery/therapist/therapistSelect";
	}

	/**
	 * 停用数据
	 */
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(RaTherapist raTherapist) {
		raTherapist.setStatus(raTherapist.STATUS_DISABLE);
		raTherapistService.updateStatus(raTherapist);
		return renderResult(Global.TRUE, text("停用成功"));
	}

	/**
	 * 启用数据
	 */
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(RaTherapist raTherapist) {
		raTherapist.setStatus(raTherapist.STATUS_NORMAL);
		raTherapistService.updateStatus(raTherapist);
		return renderResult(Global.TRUE, text("启用成功"));
	}

}