package com.jeesite.modules.recovery.question.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.question.entity.QuestionOption;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 问卷题目DAO接口
 * @author Mr Wu
 * @version 2024-09-26
 */
@MyBatisDao
public interface QuestionOptionDao extends CrudDao<QuestionOption> {
    // 查询问卷下所有选项数据
	List<QuestionOption> findOptionByQuestId(@Param("questId") String questId);
}