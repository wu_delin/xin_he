package com.jeesite.modules.recovery.chat.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import javax.validation.constraints.Size;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 咨询群组Entity
 * @author Mr Wu
 * @version 2025-02-20
 */
@Table(name="ra_chat_group", alias="a", label="咨询群组信息", columns={
		@Column(name="group_id", attrName="groupId", label="聊天群组id", isPK=true),
		@Column(name="group_name", attrName="groupName", label="群组名称", queryType=QueryType.LIKE),
		@Column(name="therapist_id", attrName="therapistId", label="治疗师id"),
		@Column(name="member_id", attrName="memberId", label="用户id"),
	}, orderBy="a.group_id DESC"
)
public class RaChatGroup extends DataEntity<RaChatGroup> {
	
	private static final long serialVersionUID = 1L;
	private String groupId;		// 聊天群组id
	private String groupName;		// 群组名称
	private String therapistId;		// 治疗师id
	private String memberId;		// 用户id

	public RaChatGroup() {
		this(null);
	}
	
	public RaChatGroup(String id){
		super(id);
	}
	
	@JsonSerialize(using = ToStringSerializer.class)
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	@Size(min=0, max=64, message="群组名称长度不能超过 64 个字符")
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

    public String getTherapistId() {
        return therapistId;
    }

    public void setTherapistId(String therapistId) {
        this.therapistId = therapistId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}