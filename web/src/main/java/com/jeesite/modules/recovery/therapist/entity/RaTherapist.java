package com.jeesite.modules.recovery.therapist.entity;


import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 治疗师列表Entity
 * @author Mr Wu
 * @version 2025-02-17
 */
@Table(name="ra_therapist", alias="a", label="治疗师列表信息", columns={
		@Column(name="therapist_id", attrName="therapistId", label="治疗师id", isPK=true),
		@Column(name="therapist_name", attrName="therapistName", label="治疗师姓名", queryType=QueryType.LIKE),
		@Column(name="therapist_sex", attrName="therapistSex", label="治疗师性别"),
		@Column(name="therapist_type", attrName="therapistType", label="治疗师类型"),
		@Column(name="therapist_phone", attrName="therapistPhone", label="治疗师电话", isUpdateForce=true),
		@Column(name="bind_openid", attrName="bindOpenid", label="绑定的openid"),
		@Column(name="status", attrName="status", label="状态", comment="状态（0正常 1删除 2停用）", isUpdate=false),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
}, orderBy="a.update_date DESC"
)
public class RaTherapist extends DataEntity<RaTherapist> {

	private static final long serialVersionUID = 1L;
	private String therapistId;		// 治疗师id
	private String therapistName;		// 治疗师姓名
	private String therapistSex;		// 治疗师性别【1男2女9未知】
	private String therapistType;		// 治疗师类型【0治疗师，1实习生】
	private String therapistPhone;		// 治疗师电话
	private String bindOpenid;		// 绑定的openid

	public RaTherapist() {
		this(null);
	}

	public RaTherapist(String id){
		super(id);
	}

	public String getTherapistId() {
		return therapistId;
	}

	public void setTherapistId(String therapistId) {
		this.therapistId = therapistId;
	}

	public String getTherapistName() {
		return therapistName;
	}

	public void setTherapistName(String therapistName) {
		this.therapistName = therapistName;
	}

	public String getTherapistSex() {
		return therapistSex;
	}

	public void setTherapistSex(String therapistSex) {
		this.therapistSex = therapistSex;
	}

	public String getTherapistType() {
		return therapistType;
	}

	public void setTherapistType(String therapistType) {
		this.therapistType = therapistType;
	}

	public String getTherapistPhone() {
		return therapistPhone;
	}

	public void setTherapistPhone(String therapistPhone) {
		this.therapistPhone = therapistPhone;
	}

	public String getBindOpenid() {
		return bindOpenid;
	}

	public void setBindOpenid(String bindOpenid) {
		this.bindOpenid = bindOpenid;
	}
}