package com.jeesite.modules.recovery.member.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.member.entity.RaMember;
import com.jeesite.modules.recovery.member.service.RaMemberService;

/**
 * 康复预约 用户表Controller
 * @author Mr Wu
 * @version 2025-02-17
 */
@Controller
@RequestMapping(value = "${adminPath}/recovery/member")
public class RaMemberController extends BaseController {

	@Autowired
	private RaMemberService raMemberService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public RaMember get(String memberId, boolean isNewRecord) {
		return raMemberService.get(memberId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(RaMember raMember, Model model) {
		model.addAttribute("raMember", raMember);
		return "modules/recovery/member/raMemberList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<RaMember> listData(RaMember raMember, HttpServletRequest request, HttpServletResponse response) {
		raMember.setPage(new Page<>(request, response));
		Page<RaMember> page = raMemberService.findPage(raMember);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(RaMember raMember, Model model) {
		model.addAttribute("raMember", raMember);
		return "modules/recovery/member/raMemberForm";
	}

	/**
	 * 保存数据
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated RaMember raMember) {
		raMemberService.save(raMember);
		return renderResult(Global.TRUE, text("保存康复预约 用户表成功！"));
	}
	
	/**
	 * 停用数据
	 */
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(RaMember raMember) {
		raMember.setStatus(RaMember.STATUS_DISABLE);
		raMemberService.updateStatus(raMember);
		return renderResult(Global.TRUE, text("停用康复预约 用户表成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(RaMember raMember) {
		raMember.setStatus(RaMember.STATUS_NORMAL);
		raMemberService.updateStatus(raMember);
		return renderResult(Global.TRUE, text("启用康复预约 用户表成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(RaMember raMember) {
		raMemberService.delete(raMember);
		return renderResult(Global.TRUE, text("删除康复预约 用户表成功！"));
	}
}