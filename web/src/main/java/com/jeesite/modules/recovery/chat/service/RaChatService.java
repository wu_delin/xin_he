package com.jeesite.modules.recovery.chat.service;

import java.util.Date;
import java.util.List;

import com.jeesite.common.lang.DateUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.recovery.chat.dao.RaChatGroupDao;
import com.jeesite.modules.recovery.chat.entity.RaChatGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.chat.entity.RaChat;
import com.jeesite.modules.recovery.chat.dao.RaChatDao;

import javax.validation.constraints.Size;

/**
 * 聊天Service
 * @author Mr Wu
 * @version 2025-02-20
 */
@Service
public class RaChatService extends CrudService<RaChatDao, RaChat> {
	@Autowired
	private RaChatGroupDao groupDao;

	/**
	 * 获取单条数据
	 * @param raChat
	 * @return
	 */
	@Override
	public RaChat get(RaChat raChat) {
		return super.get(raChat);
	}
	
	/**
	 * 查询分页数据
	 * @param raChat 查询条件
	 * @param raChat page 分页对象
	 * @return
	 */
	@Override
	public Page<RaChat> findPage(RaChat raChat) {
		return super.findPage(raChat);
	}
	
	/**
	 * 查询列表数据
	 * @param raChat
	 * @return
	 */
	@Override
	public List<RaChat> findList(RaChat raChat) {
		return super.findList(raChat);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param raChat
	 */
	@Override
	@Transactional
	public void save(RaChat raChat) {
		super.save(raChat);
	}
	
	/**
	 * 更新状态
	 * @param raChat
	 */
	@Override
	@Transactional
	public void updateStatus(RaChat raChat) {
		super.updateStatus(raChat);
	}
	
	/**
	 * 删除数据
	 * @param raChat
	 */
	@Override
	@Transactional
	public void delete(RaChat raChat) {
		super.delete(raChat);
	}

	/**
	 * 查询群列表
	 * @param raChat
	 */
	public Page<RaChat> findGroupList(RaChat raChat) {
        List<RaChat> groupList = dao.findGroupList(raChat);
        for (RaChat chat : groupList) {
            String sendMsg = chat.getSendMsg();
            String[] split = sendMsg.split("##");
            chat.setSenderId(split[0]);
            chat.setSenderDate(DateUtils.parseDate(split[1]));
            chat.setSendMsg(split[2]);
        }
        Page<RaChat> page = raChat.getPage().setList(groupList);
		return page;
	}

	/**
	 * 更新为已读
	 * @param raChat
	 */
	@Transactional
	public void updateUnread(RaChat raChat) {
		dao.updateUnread(raChat);
	}

	/**
	 * 群服务
	 * @param raChatGroup
	 */
	@Transactional
	public String serviceMemberGroup(RaChatGroup raChatGroup) {
		String code = groupDao.getMemberGroup(raChatGroup);		// 查询群
		if(StringUtils.isEmpty(code)){
			raChatGroup.setCreateDate(new Date());
			groupDao.insert(raChatGroup);		// 新增群
		}
		RaChat raChat = new RaChat();
		raChat.setGroupId(raChatGroup.getGroupId());
		raChat.setSenderId(raChatGroup.getMemberId());
		raChat.setSenderDate(new Date());
		raChat.setSenderRole("member");
		raChat.setSendMsg("您好，我预约了一个信息，请即时帮我处理一下！");
		raChat.setUnread("0");
		dao.insert(raChat);		// 新增聊天信息
		return null;
	}

	/**
	 * 查询未读消息条数
	 * @param senderId
	 */
	public Integer getUnreadTotal(String senderId) {
		return dao.getUnreadTotal(senderId);
	}
}