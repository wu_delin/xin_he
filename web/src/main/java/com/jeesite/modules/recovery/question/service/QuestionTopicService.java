package com.jeesite.modules.recovery.question.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.question.dao.QuestionOptionDao;
import com.jeesite.modules.recovery.question.dao.QuestionTopicDao;
import com.jeesite.modules.recovery.question.entity.QuestionOption;
import com.jeesite.modules.recovery.question.entity.QuestionTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 问卷题目Service
 * @author Mr Wu
 * @version 2024-09-26
 */
@Service
public class QuestionTopicService extends CrudService<QuestionTopicDao, QuestionTopic> {
	
	@Autowired
	private QuestionOptionDao questionOptionDao;
	
	/**
	 * 获取单条数据
	 * @param questionTopic
	 * @return
	 */
	@Override
	public QuestionTopic get(QuestionTopic questionTopic) {
		QuestionTopic entity = super.get(questionTopic);
		if (entity != null){
			QuestionOption questionOption = new QuestionOption(entity);
			questionOption.setStatus(QuestionOption.STATUS_NORMAL);
			entity.setQuestionOptionList(questionOptionDao.findList(questionOption));
		}
		return entity;
	}
	
	/**
	 * 查询分页数据
	 * @param questionTopic 查询条件
	 * @param questionTopic page 分页对象
	 * @return
	 */
	@Override
	public Page<QuestionTopic> findPage(QuestionTopic questionTopic) {
		return super.findPage(questionTopic);
	}
	
	/**
	 * 查询列表数据
	 * @param questionTopic
	 * @return
	 */
	@Override
	public List<QuestionTopic> findList(QuestionTopic questionTopic) {
		return super.findList(questionTopic);
	}

	/**
	 * 保存数据（插入或更新）
	 * @param questionTopic
	 */
	@Override
	@Transactional
	public void save(QuestionTopic questionTopic) {
        questionTopic.setTopicRule(questionTopic.getTopicRule().replaceAll("＜","<").replaceAll("＞",">"));
		super.save(questionTopic);
		// 保存 QuestionTopic子表
		for (QuestionOption questionOption : questionTopic.getQuestionOptionList()){
		    if (!QuestionOption.STATUS_DELETE.equals(questionOption.getStatus())){
				questionOption.setTopicId(questionTopic);
				if (questionOption.getIsNewRecord()){
					questionOptionDao.insert(questionOption);
				}else{
					questionOptionDao.update(questionOption);
				}
			}else{
				questionOptionDao.delete(questionOption);
			}
		}
	}
	
	/**
	 * 更新状态
	 * @param questionTopic
	 */
	@Override
	@Transactional
	public void updateStatus(QuestionTopic questionTopic) {
		super.updateStatus(questionTopic);
	}
	
	/**
	 * 删除数据
	 * @param questionTopic
	 */
	@Override
	@Transactional
	public void delete(QuestionTopic questionTopic) {
		super.delete(questionTopic);
		QuestionOption questionOption = new QuestionOption();
		questionOption.setTopicId(questionTopic);
		questionOptionDao.deleteByEntity(questionOption);
	}

    /**
     * 查询问卷下所有选项数据
     */
    public List<QuestionOption> findOptionByQuestId(String questId) {
        return questionOptionDao.findOptionByQuestId(questId);
    }

}