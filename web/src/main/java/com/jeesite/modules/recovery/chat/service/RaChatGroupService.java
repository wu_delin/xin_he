package com.jeesite.modules.recovery.chat.service;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recovery.chat.entity.RaChatGroup;
import com.jeesite.modules.recovery.chat.dao.RaChatGroupDao;

/**
 * 咨询群组Service
 * @author Mr Wu
 * @version 2025-02-20
 */
@Service
public class RaChatGroupService extends CrudService<RaChatGroupDao, RaChatGroup> {
	
	/**
	 * 获取单条数据
	 * @param raChatGroup
	 * @return
	 */
	@Override
	public RaChatGroup get(RaChatGroup raChatGroup) {
		return super.get(raChatGroup);
	}
	
	/**
	 * 查询分页数据
	 * @param raChatGroup 查询条件
	 * @param raChatGroup page 分页对象
	 * @return
	 */
	@Override
	public Page<RaChatGroup> findPage(RaChatGroup raChatGroup) {
		return super.findPage(raChatGroup);
	}
	
	/**
	 * 查询列表数据
	 * @param raChatGroup
	 * @return
	 */
	@Override
	public List<RaChatGroup> findList(RaChatGroup raChatGroup) {
		return super.findList(raChatGroup);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param raChatGroup
	 */
	@Override
	@Transactional
	public void save(RaChatGroup raChatGroup) {
		super.save(raChatGroup);
	}
	
	/**
	 * 更新状态
	 * @param raChatGroup
	 */
	@Override
	@Transactional
	public void updateStatus(RaChatGroup raChatGroup) {
		super.updateStatus(raChatGroup);
	}
	
	/**
	 * 删除数据
	 * @param raChatGroup
	 */
	@Override
	@Transactional
	public void delete(RaChatGroup raChatGroup) {
		super.delete(raChatGroup);
	}
	
}