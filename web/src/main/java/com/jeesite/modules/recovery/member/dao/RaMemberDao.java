package com.jeesite.modules.recovery.member.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recovery.member.entity.RaMember;

import java.util.List;

/**
 * 康复预约 用户表DAO接口
 * @author Mr Wu
 * @version 2025-02-17
 */
@MyBatisDao
public interface RaMemberDao extends CrudDao<RaMember> {
    // 查询登录信息【api】
    List<RaMember> getLoginInfo(RaMember raMember);
}