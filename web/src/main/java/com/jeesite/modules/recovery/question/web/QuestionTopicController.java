package com.jeesite.modules.recovery.question.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.question.entity.QuestionTopic;
import com.jeesite.modules.recovery.question.service.QuestionTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 问卷题目Controller
 * @author Mr Wu
 * @version 2024-09-26
 */
@Controller
@RequestMapping(value = "${adminPath}/question/questionTopic")
public class QuestionTopicController extends BaseController {

	@Autowired
	private QuestionTopicService questionTopicService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public QuestionTopic get(String topicId, boolean isNewRecord) {
		return questionTopicService.get(topicId, isNewRecord);
	}

    /**
     * 问卷题目 -- index
     */
    @RequestMapping(value = "index")
    public String index(QuestionTopic questionTopic, Model model) {
        model.addAttribute("questionTopic", questionTopic);
        return "modules/recovery/question/questionTopicIndex";
    }

	/**
	 * 问卷题目 -- list
	 */
	@RequestMapping(value = "list")
	public String list(QuestionTopic questionTopic, Model model) {
		model.addAttribute("questionTopic", questionTopic);
		return "modules/recovery/question/questionTopicList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<QuestionTopic> listData(QuestionTopic questionTopic, HttpServletRequest request, HttpServletResponse response) {
		questionTopic.setPage(new Page<>(request, response));
		Page<QuestionTopic> page = questionTopicService.findPage(questionTopic);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(QuestionTopic questionTopic, Model model) {
        if (questionTopic.getIsNewRecord()) {
            long count = questionTopicService.findCount(questionTopic);
            questionTopic.setTopicNumber(count+1);
        }
		model.addAttribute("questionTopic", questionTopic);
		return "modules/recovery/question/questionTopicForm";
	}

	/**
	 * 保存数据
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated QuestionTopic questionTopic) {
		questionTopicService.save(questionTopic);
		return renderResult(Global.TRUE, text("保存问卷题目成功！"));
	}
	
	/**
	 * 停用数据
	 */
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(QuestionTopic questionTopic) {
		questionTopic.setStatus(QuestionTopic.STATUS_DISABLE);
		questionTopicService.updateStatus(questionTopic);
		return renderResult(Global.TRUE, text("停用问卷题目成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(QuestionTopic questionTopic) {
		questionTopic.setStatus(QuestionTopic.STATUS_NORMAL);
		questionTopicService.updateStatus(questionTopic);
		return renderResult(Global.TRUE, text("启用问卷题目成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(QuestionTopic questionTopic) {
		questionTopicService.delete(questionTopic);
		return renderResult(Global.TRUE, text("删除问卷题目成功！"));
	}
	
}