package com.jeesite.modules.recovery.question.web;

import com.alibaba.fastjson.JSON;
import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.collect.MapUtils;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.question.entity.Question;
import com.jeesite.modules.recovery.question.service.QuestionTopicService;
import com.jeesite.modules.recovery.question.entity.QuestionOption;
import com.jeesite.modules.recovery.question.entity.QuestionTopic;
import com.jeesite.modules.recovery.question.service.QuestionService;
import com.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 问卷管理Controller
 * @author qu
 * @version 2024-09-10
 */
@Controller
@RequestMapping(value = "${adminPath}/question")
public class QuestionController extends BaseController {

	@Autowired
	private QuestionService questionService;

    @Autowired
    private QuestionTopicService questionTopicService;

	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Question get(String questId, boolean isNewRecord) {
		return questionService.get(questId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "list")
	public String list(Question question, Model model) {
		model.addAttribute("question", question);
		return "modules/recovery/question/questionList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Question> listData(Question question, HttpServletRequest request, HttpServletResponse response) {
		question.setPage(new Page<>(request, response));
		Page<Question> page = questionService.findPage(question);
		return page;
	}

	/**
	 * 查询问卷记录列表
	 */
	@RequestMapping(value = "questionRecord")
	public String questionRecord(Question question, Model model) {
		model.addAttribute("question", question);
		return "modules/recovery/question/questionRecord";
	}

	/**
	 * 查询问卷记录详情
	 */
	@RequestMapping(value = "getRecordDetail")
	public String getRecordDetail(String scoreId, Model model) {
		Map data = new HashMap();
		data.put("scoreId",scoreId);
		data = questionService.getRecordDetail(data);
		if(!Objects.isNull(data.get("questId"))){
			if("34".equals(data.get("questId"))){
				Object attr = JSON.parse(data.get("resultInfo").toString());
				data.put("infoList",attr);
			}
		}
		model.addAttribute("record", data);
		return "modules/recovery/question/questionDetail";
	}

	/**
	 * 更新问卷信息
	 */
	@RequestMapping(value = "updateQuestionRecord")
	@ResponseBody
	public String updateQuestionRecord(HttpServletRequest request) {
		Map map = new HashMap();
		map.put("scoreId",request.getParameter("scoreId"));
		map.put("doctorName", UserUtils.getUser().getUserName());
		map.put("doctorAdvice",request.getParameter("doctorAdvice"));
		questionService.updateQuestionRecord(map);
		return renderResult(Global.TRUE, text("保存问卷管理成功！"));
	}
	/**
	 * 查询问卷记录数据
	 */
	@RequestMapping(value = "findRecordList")
	@ResponseBody
	public Page<Map> findRecordList(HttpServletRequest request, HttpServletResponse response) {
		Map map = new HashMap();
		map.put("memberName",request.getParameter("memberName"));
		map.put("questName",request.getParameter("questName"));
		map.put("questType",request.getParameter("questType"));
		map.put("doctorName",request.getParameter("doctorName"));
		Page<Map> page = questionService.findRecordList(new Page<>(request, response),map);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(Question question, Model model) {
		model.addAttribute("question", question);
		return "modules/recovery/question/questionForm";
	}

	/**
	 * 保存数据
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Question question) {
		questionService.save(question);
		return renderResult(Global.TRUE, text("保存问卷管理成功！"));
	}
	
	/**
	 * 停用数据
	 */
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Question question) {
		question.setStatus(Question.STATUS_DISABLE);
		questionService.updateStatus(question);
		return renderResult(Global.TRUE, text("停用问卷管理成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Question question) {
		question.setStatus(Question.STATUS_NORMAL);
		questionService.updateStatus(question);
		return renderResult(Global.TRUE, text("启用问卷管理成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Question question) {
		questionService.delete(question);
		return renderResult(Global.TRUE, text("删除问卷管理成功！"));
	}

    /**
     * 查询问卷树结构
     */
    @RequestMapping(value = "questionTree")
    @ResponseBody
    public List<Map> questionTree(Question question) {
        List<Question> list = questionService.findList(question);
        List<Map> mapList = ListUtils.newArrayList();
        list.forEach(e -> {
            Map<String, Object> map = MapUtils.newHashMap();
            map.put("id", e.getId());
            map.put("pId", "0");
            map.put("code", e.getQuestId());
            map.put("name", e.getQuestName());
            mapList.add(map);
        });
        return mapList;
    }

    /**
     * 预览问卷
     */
    @RequestMapping(value = "viewQuestion")
    public String viewQuestion() {
        return "modules/recovery/question/viewQuestion";
    }

    /**
     * 获取问卷详情
     */
    @RequestMapping(value = "findQuestionDetail")
    @ResponseBody
    public Map findQuestionDetail(QuestionTopic questionTopic) {
        Question question = questionService.get(questionTopic.getQuestId());
        List<QuestionTopic> list = questionTopicService.findList(questionTopic);
        List<QuestionOption> optionList = questionTopicService.findOptionByQuestId(questionTopic.getQuestId());
        Map<String, List<QuestionOption>> mapList = optionList.stream().collect(Collectors.groupingBy(QuestionOption::getTopicId2));
        for (QuestionTopic topic : list) {
            topic.setQuestionOptionList(mapList.get(topic.getTopicId()));
        }
        Map map = new HashMap();
        map.put("question",question);
        map.put("questList",list);
        return map;
    }
}