package com.jeesite.modules.recovery.question.entity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;

/**
 * 问卷管理 Entity
 * @author jeffery
 * @version 2024-10-14
 */
@Table(name="ra_ques_score", alias="a", label="问卷得分表", columns={
        @Column(name="score_id", attrName="scoreId", label="主键id", isPK=true),
        @Column(name="member_code", attrName="memberCode", label="用户id"),
        @Column(name="quest_id", attrName="questId", label="问卷id"),
        @Column(name="total_score", attrName="totalScore", label="问卷总分"),
        @Column(name="result_info", attrName="resultInfo", label="结果描述"),
        @Column(name="health_advice", attrName="healthAdvice", label="健康建议"),
        @Column(name="answer_time", attrName="answerTime", label="答题时长"),
        @Column(name="doctor_advice", attrName="doctorAdvice", label="医生建议"),
        @Column(name="doctor_name", attrName="doctorName", label="医生名字"),
        @Column(name="advice_date", attrName="adviceDate", label="建议日期"),
        @Column(name="state", attrName="state", label="状态（0医生未处理 1已处理）"),
        @Column(name="remarks", attrName="remarks", label="备注信息"),
        @Column(name="create_date", attrName="createDate", label="完成日期"),
}, orderBy="a.create_date DESC")
public class Ques extends DataEntity<Ques> {
    private static final long serialVersionUID = 1L;
    private String questId;		// 调查问卷ID
    private String scoreId;       // 主键id
    private String memberCode;    // 用户id
    private String totalScore;    // 问卷总分
    private String resultInfo;    // 结果描述
    private String healthAdvice;  // 健康建议
    private String answerTime;    // 答题时长
    private String doctorAdvice;  // 医生建议
    private String doctorName;    // 医生名字
    private String adviceDate;    // 建议日期
    private String state;         // 医生处理状态
    private String factorTotal;   // 问卷最小值

    public Ques() {
        this(null);
    }

    public Ques(String id){
        super(id);
    }

    public String getQuestId() {
        return questId;
    }

    public void setQuestId(String questId) {
        this.questId = questId;
    }

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public String getResultInfo() {
        return resultInfo;
    }

    public void setResultInfo(String resultInfo) {
        this.resultInfo = resultInfo;
    }

    public String getHealthAdvice() {
        return healthAdvice;
    }

    public void setHealthAdvice(String healthAdvice) {
        this.healthAdvice = healthAdvice;
    }

    public String getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(String answerTime) {
        this.answerTime = answerTime;
    }

    public String getDoctorAdvice() {
        return doctorAdvice;
    }

    public void setDoctorAdvice(String doctorAdvice) {
        this.doctorAdvice = doctorAdvice;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getAdviceDate() {
        return adviceDate;
    }

    public void setAdviceDate(String adviceDate) {
        this.adviceDate = adviceDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFactorTotal() {
        return factorTotal;
    }

    public void setFactorTotal(String factorTotal) {
        this.factorTotal = factorTotal;
    }
}
