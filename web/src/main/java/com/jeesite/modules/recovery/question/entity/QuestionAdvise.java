package com.jeesite.modules.recovery.question.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 问卷建议表Entity
 * @author Mr Wu
 * @version 2024-10-16
 */
@Table(name="ra_ques_advise", alias="a", label="问卷建议表信息", columns={
		@Column(name="advice_id", attrName="adviceId", label="主键id", isPK=true),
		@Column(name="quest_id", attrName="questId", label="问卷id"),
		@Column(name="advice_low", attrName="adviceLow", label="低值"),
		@Column(name="advice_high", attrName="adviceHigh", label="高值"),
		@Column(name="content", attrName="content", label="内容"),
		@Column(name="advice_info", attrName="adviceInfo", label="健康建议"),
	}, orderBy="a.advice_id asc"
)
public class QuestionAdvise extends DataEntity<QuestionAdvise> {
	
	private static final long serialVersionUID = 1L;
	private Long adviceId;		// 主键id
	private String questId;		// 问卷id
	private Double adviceLow;		// 低值
	private Double adviceHigh;		// 高值
	private String content;		// 内容
	private String adviceInfo;		// 健康建议
	private List<QuestionAdvise> dataList;		// 健康建议

	public QuestionAdvise() {
		this(null);
	}
	
	public QuestionAdvise(String id){
		super(id);
	}

    public List<QuestionAdvise> getDataList() {
        return dataList;
    }

    public void setDataList(List<QuestionAdvise> dataList) {
        this.dataList = dataList;
    }

    @JsonSerialize(using = ToStringSerializer.class)
	public Long getAdviceId() {
		return adviceId;
	}

	public void setAdviceId(Long adviceId) {
		this.adviceId = adviceId;
	}
	
	@NotNull(message="问卷id不能为空")
	public String getQuestId() {
		return questId;
	}

	public void setQuestId(String questId) {
		this.questId = questId;
	}
	
	public Double getAdviceLow() {
		return adviceLow;
	}

	public void setAdviceLow(Double adviceLow) {
		this.adviceLow = adviceLow;
	}
	
	public Double getAdviceHigh() {
		return adviceHigh;
	}

	public void setAdviceHigh(Double adviceHigh) {
		this.adviceHigh = adviceHigh;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getAdviceInfo() {
		return adviceInfo;
	}

	public void setAdviceInfo(String adviceInfo) {
		this.adviceInfo = adviceInfo;
	}
	
}