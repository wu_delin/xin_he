package com.jeesite.modules.recovery.apply.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.recovery.therapist.entity.RaTherapist;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 申请 康复治疗预约Entity
 * @author Mr Wu
 * @version 2025-02-17
 */
@Table(name="ra_apply", alias="a", label="申请 康复治疗预约信息", columns={
		@Column(name="apply_id", attrName="applyId", label="预约id", isPK=true),
		@Column(name="member_id", attrName="memberId", label="用户id"),
		@Column(name="therapist_id", attrName="therapistId", label="治疗师"),
		@Column(name="am_day", attrName="amDay", label="预约日期"),
		@Column(name="am_schedule", attrName="amSchedule", label="预约节点"),
		@Column(name="apply_date", attrName="applyDate", label="申请预约时间"),
		@Column(name="apply_state", attrName="applyState", label="申请预约状态", comment = "0已申请1已取消2已完成"),
		@Column(name="heal_data", attrName="healData", label="治疗数据"),
		@Column(name="description", attrName="description", label="描述信息")
	},joinTable={
        @JoinTable(type= JoinTable.Type.LEFT_JOIN, entity= RaTherapist.class, attrName="this", alias="t",
                on="t.therapist_id = a.therapist_id", columns={
                @Column(name="therapist_name", attrName="therapistName", label="治疗师姓名", queryType= QueryType.LIKE),
        }),
    }, orderBy="a.apply_id DESC"
)
public class RaApply extends DataEntity<RaApply> {
	
	private static final long serialVersionUID = 1L;
	private String applyId;		// 预约id
	private String memberId;    // 用户id
	private String therapistId; // 治疗师
	private String therapistName; // 治疗师
	private String amDay;	// 预约日期
	private String amSchedule;	// 预约节点
	private Date applyDate;		// 申请预约时间
	private String applyState;	// 申请预约状态
	private String healData;	// 治疗数据
	private String description;	// 描述信息
	private String planContent;	// 运动训练方案内容
	private String scheduleId;		// 排班计划id

	public RaApply() {
		this(null);
	}
	
	public RaApply(String id){
		super(id);
	}

    public String getPlanContent() {
        return planContent;
    }

    public void setPlanContent(String planContent) {
        this.planContent = planContent;
    }

    public String getAmDay() {
        return amDay;
    }

    public void setAmDay(String amDay) {
        this.amDay = amDay;
    }

    public String getTherapistName() {
        return therapistName;
    }

    public void setTherapistName(String therapistName) {
        this.therapistName = therapistName;
    }

    public String getHealData() {
        return healData;
    }

    public void setHealData(String healData) {
        this.healData = healData;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
	
	@Size(min=0, max=64, message="治疗师长度不能超过 64 个字符")
	public String getTherapistId() {
		return therapistId;
	}

	public void setTherapistId(String therapistId) {
		this.therapistId = therapistId;
	}
	
	@Size(min=0, max=1, message="预约节点长度不能超过 1 个字符")
	public String getAmSchedule() {
		return amSchedule;
	}

	public void setAmSchedule(String amSchedule) {
		this.amSchedule = amSchedule;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	
	@Size(min=0, max=1, message="申请预约状态长度不能超过 1 个字符")
	public String getApplyState() {
		return applyState;
	}

	public void setApplyState(String applyState) {
		this.applyState = applyState;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}