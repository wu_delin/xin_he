package com.jeesite.modules.recovery.plan.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recovery.plan.entity.Plan;
import com.jeesite.modules.recovery.plan.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 方案管理Controller
 * @author qu
 * @version 2025-02-21
 */
@Controller
@RequestMapping(value = "${adminPath}/plan/plan")
public class PlanController extends BaseController {

	@Autowired
	private PlanService planService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Plan get(String planId, boolean isNewRecord) {
		return planService.get(planId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */

	@RequestMapping(value = {"list", ""})
	public String list(Plan plan, Model model) {
		model.addAttribute("plan", plan);
		return "modules/recovery/plan/planList";
	}
	
	/**
	 * 查询列表数据
	 */

	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Plan> listData(Plan plan, HttpServletRequest request, HttpServletResponse response) {
		plan.setPage(new Page<>(request, response));
		Page<Plan> page = planService.findPage(plan);
		return page;
	}

	/**
	 * 查看编辑表单
	 */

	@RequestMapping(value = "form")
	public String form(Plan plan, Model model) {
		model.addAttribute("plan", plan);
		return "modules/recovery/plan/planForm";
	}

	/**
	 * 保存数据
	 */

	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Plan plan) {
		planService.save(plan);
		return renderResult(Global.TRUE, text("保存方案管理成功！"));
	}
	
	/**
	 * 停用数据
	 */

	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Plan plan) {
		plan.setStatus(Plan.STATUS_DISABLE);
		planService.updateStatus(plan);
		return renderResult(Global.TRUE, text("停用方案管理成功"));
	}
	
	/**
	 * 启用数据
	 */

	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Plan plan) {
		plan.setStatus(Plan.STATUS_NORMAL);
		planService.updateStatus(plan);
		return renderResult(Global.TRUE, text("启用方案管理成功"));
	}
	
	/**
	 * 删除数据
	 */

	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Plan plan) {
		planService.delete(plan);
		return renderResult(Global.TRUE, text("删除方案管理成功！"));
	}
	
}