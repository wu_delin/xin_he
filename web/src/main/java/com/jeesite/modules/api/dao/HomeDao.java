package com.jeesite.modules.api.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.task.entity.Task;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@MyBatisDao
public interface HomeDao extends CrudDao<Task> {

    List<Map<String, Object>> findMyWaitHandle(Map map);

    Map getTaskDetailData(@Param("taskId") String taskId);

    List<Map> findTaskPage(Map map);

    List<Task> findTaskRelease(Task task);

    List<Map> findTaskRecord(Map map);

    List<Map> getUserInfo(Map map);

    List<Map> findNotice(Map map);

    List<Map> findImgList();

    String getThisUserNick(Map map);

    int updateUserPassword(Map map);

    List<Map> findThreeNoticeList();

    int updateMemberInfo(Member member);

    List<Map> findPointsTotal(@Param("memberCode") String memberCode);

    List<Map> findPointsTotalPage(Map map);

    List<Map> getPointsData(Map map);

    List<Map> findUserInfoChange(Map map);

    Map getCompletedDetail(@Param("taskId") String taskId);
}
