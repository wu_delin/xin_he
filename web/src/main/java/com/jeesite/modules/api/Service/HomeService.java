package com.jeesite.modules.api.Service;

import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.codec.AesUtils;
import com.jeesite.common.collect.MapUtils;
import com.jeesite.common.entity.Page;
import com.jeesite.common.lang.DateUtils;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.api.dao.HomeDao;
import com.jeesite.modules.apply.dao.ApplyDao;
import com.jeesite.modules.apply.entity.Apply;
import com.jeesite.modules.general.CosUtil;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.sys.entity.DictData;
import com.jeesite.modules.sys.utils.DictUtils;
import com.jeesite.modules.task.entity.Task;
import com.jeesite.modules.task.service.TaskService;
import org.apache.poi.hwpf.model.ListData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
public class HomeService extends CrudService<HomeDao, Task> {

    @Autowired
    private ApplyDao applyDao;

    /**
     * 查询-我的待办-数据
     */
    public Page<Map<String, Object>> findMyWaitHandle(Integer pageNo, Integer pageSize,String taskName, String memberCode){
        Page<Map<String, Object>> pageMap = new Page<>();
        Map<String, Object> params = MapUtils.newHashMap();
        params.put("taskName", taskName);
        params.put("memberCode", memberCode);
        pageMap.setPageNo(pageNo);
        pageMap.setPageSize(pageSize);
        pageMap.setOrderBy("end_date asc");
        params.put("page", pageMap);
        List<Map<String, Object>> listData = dao.findMyWaitHandle(params);
        for (Map data : listData) {
            data.put("taskImg", CosUtil.URL+data.get("taskImg").toString());
        }
        pageMap.setList(listData);
        return pageMap;
    }

    /**
     * 首页我的待办跳转后查询任务详情
     */
    public Map getTaskDetailData(String taskId){
        Map map = dao.getTaskDetailData(taskId);
        if (map.get("taskImg") != null){
            map.put("taskImg", CosUtil.URL+map.get("taskImg").toString());
        }
        return map;
    }

    /**
     * 分页查询待办任务
     */
    public Page<Map> findTaskPage(Page<Map> page,String taskName,String memberCode){
        Map map1 = new HashMap();
        map1.put("taskName",taskName);
        map1.put("memberCode",memberCode);
        page.setOrderBy("end_date asc");
        map1.put("page",page);
        List<Map> taskPage = dao.findTaskPage(map1);
        for (Map map : taskPage) {
            map.put("taskImg",CosUtil.URL+map.get("taskImg"));
        }
        return page.setList(taskPage);
    }

    /**
     * 任务发布
     */
    public Page<Task> findTaskRelease(Task task){
        Page<Task> page = task.getPage();
        List<Task> taskRelease = dao.findTaskRelease(task);
        for (Task task1:taskRelease){
            task1.setTaskImg(CosUtil.URL+task.getTaskImg());
        }
        page.setList(taskRelease);
        return page;
    }

    /**
     * 任务审核
     */
    public Page<Map> findTaskRecord(Page<Map> page,Map map){
        map.put("page",page);
        List<Map> taskRecord = dao.findTaskRecord(map);
        List<DictData> dictList = DictUtils.getDictList("sys_user_sex");
        for (Map map1 :taskRecord){
            for (DictData data:dictList){
                if (data.getDictValue().equals(map1.get("gender"))){
                    map1.put("genderName",data.getDictLabelRaw());
                }
            }
            map1.put("taskImg",CosUtil.URL+map1.get("taskImg"));
        }
        return page.setList(taskRecord);
    }

    /**
     * 用户登录
     */
    public Map getUserInfo(String nick,String password){
        Map map = new HashMap();
        //密码加密
        String encode = AesUtils.encode(password);
        map.put("nick",nick);
        map.put("password",encode);
        List<Map> userInfo = dao.getUserInfo(map);
        if (userInfo.size() != 1){
            Map map1 = new HashMap();
            map1.put("res",false);
            map1.put("msg","您输入的信息有误，请重新输入！");
            return map1;
        }
        Map user = userInfo.get(0);
        user.put("res",true);
        List<DictData> userSex = DictUtils.getDictList("sys_user_sex");
        for (DictData data:userSex){
            if (data.getDictValue().equals(user.get("gender").toString())){
                user.put("genderName",data.getDictLabelRaw());
            }
        }
        user.put("phone",AesUtils.decode(user.get("phone").toString()));
        user.put("password",AesUtils.decode(user.get("password").toString()));
        user.put("cardNo",AesUtils.decode(user.get("cardNo").toString()));
        // 计算年龄
        Object birthday1 = user.get("birthday");
        if(!Objects.isNull(birthday1)){
            Date birthday = (Date) user.get("birthday");
            Calendar birthCalendar = Calendar.getInstance();
            birthCalendar.setTime(birthday);
            Calendar currentCalendar = Calendar.getInstance();
            int age = currentCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);
            if (currentCalendar.get(Calendar.DAY_OF_YEAR) < birthCalendar.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }
            user.put("age",age);
        }else {
            user.put("age","");
        }
        return user;
    }


    /**
     * 公告信息
     */
    public Page<Map> findNotice(Page<Map> page){
        Map map = new HashMap();
        map.put("page",page);
        List<Map> notice = dao.findNotice(map);
        for (Map map1:notice){
            map1.put("annexUrl",CosUtil.URL+map1.get("annexUrl"));
        }
        return page.setList(notice);
    }

    /**
     * 轮播图图片链接
     * (只返回前六条)
     */
    public List<Map> findImgList(){
        List<Map> imgList = dao.findImgList();
        for (Map map:imgList){
            map.put("imgUrl",CosUtil.URL+map.get("imgUrl"));
        }
        return imgList;
    }

    /**
     * 修改密码
     */
    public JSONObject updatePassword(String memberCode,String nick,String oldPassword,String newPassword){
        JSONObject json = new JSONObject();
        Map map = new HashMap();
        map.put("memberCode",memberCode);
        map.put("nick",nick);
        map.put("password",AesUtils.encode(oldPassword));
        String count = dao.getThisUserNick(map);
        if ("".equals(count)){
            json.put("res",false);
            json.put("msg","输入的信息有误，请重新输入!");
            return json;
        }
        map.put("newPassword",AesUtils.encode(newPassword));
        int i = dao.updateUserPassword(map);
        if (i == 1){
            json.put("res",true);
            json.put("msg","修改成功！");
        }else{
            json.put("res",false);
            json.put("msg","修改失败，请重试！");
        }
        return json;
    }

    /**
     * 查询三条公告信息，展示到首页公告
     */
    public List<Map> findThreeNoticeList(){
        return dao.findThreeNoticeList();
    }


    /**
     * 查询字典数据
     */
    public List<Map> findDictList(String dictName){
        List<DictData> dictList = DictUtils.getDictList(dictName);
        List<Map> mapList = new ArrayList<>();
        for (DictData data:dictList){
            Map map = new HashMap();
            map.put("dictValue",data.getDictValue());
            map.put("dictLabelRow",data.getDictLabelRaw());
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * 修改用户信息
     */
    public void updateMemberInfo(Member member){
        dao.updateMemberInfo(member);
    }

    /**
     * 积分申请--保存数据
     * @param apply
     */
    @Transactional
    public void saveIntegral(Apply apply) {
        if (apply.getIsNewRecord()) {
            applyDao.insert(apply);
        }else {
            applyDao.update(apply);
        }
        String deleteFile = apply.getDeleteFile();
        if (!"".equals(deleteFile)) {
            CosUtil.delCOSFile(deleteFile,"xh/apply/file");
        }
        String deleteImg = apply.getDeleteImg();
        if (!"".equals(deleteImg)) {
            CosUtil.delCOSFile(deleteImg,"xh/apply/img");
        }
    }

    /**
     * 查询积分（总数）
     */
    public List<Map> findPointsTotal(String memberCode){
        List<Map> pointsTotal = dao.findPointsTotal(memberCode);
        return pointsTotal;
    }

    /**
     * 分页查询积分列表
     */
    public Page<Map> findPointsTotalPage(Page<Map> page,String memberCode){
        Map map = new HashMap();
        page.setOrderBy("p.create_date desc");
        map.put("page",page);
        map.put("memberCode",memberCode);
        List<Map> totalPage = dao.findPointsTotalPage(map);
        for (Map map1:totalPage){
            if (!"9".equals(map1.get("pointsType").toString())){
                map1.put("pointsNum","+"+map1.get("pointsNum"));
            }
            map1.put("pointsType", DictUtils.getDictLabel("point_type",map1.get("pointsType").toString(),"-"));
        }
        return page.setList(totalPage);
    }

    /**
     * 积分页面，查询上半部分数据
     * 1：完成任务的次数
     * 2：总积分
     * 3：今日积分
     */
    public List<Map> getPointsData(String memberCode){
        Map map = new HashMap();
        map.put("memberCode",memberCode);
        Date startDate = DateUtils.getOfDayFirst(new Date());
        Date endDate = DateUtils.getOfDayLast(new Date());
        map.put("startDate",startDate);
        map.put("endDate",endDate);
        return dao.getPointsData(map);
    }

    /**
     * 查询用户列表信息
     */
    public Page<Map> findUserInfoChange(String content, Page<Map> page){
        Map map = new HashMap();
        map.put("page",page);
        map.put("content",content);
        List<Map> userInfoChange = dao.findUserInfoChange(map);
        for (Map map1:userInfoChange){
            map1.put("cardNo",AesUtils.decode(map1.get("cardNo").toString()));
            map1.put("phone",AesUtils.decode(map1.get("phone").toString()));
            map1.put("gender",DictUtils.getDictValue("sys_user_sex",map1.get("gender").toString(),"-"));
        }
        page.setList(userInfoChange);
        return page;
    }

    /**
     * 查询已办任务数据
     */
    public Map getCompletedDetail(String taskId){
        Map detail = dao.getCompletedDetail(taskId);
        detail.put("taskImg",CosUtil.URL+detail.get("taskImg"));
        String[] submitImgs = detail.get("submitImg").toString().split(",");
        String imgs = "";
        if (!"".equals(submitImgs)){
            for (String str:submitImgs){
                if ("".equals(imgs)){
                    imgs = CosUtil.URL+str;
                }else {
                    imgs = imgs+","+(CosUtil.URL+str);
                }
            }
            detail.put("submitImg",imgs);
        }
        return detail;
    }
}
