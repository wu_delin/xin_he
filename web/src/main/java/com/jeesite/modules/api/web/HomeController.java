package com.jeesite.modules.api.web;

import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.api.Service.HomeService;
import com.jeesite.modules.apply.entity.Apply;
import com.jeesite.modules.apply.service.ApplyService;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.penalize.entity.Penalize;
import com.jeesite.modules.penalize.service.PenalizeService;
import com.jeesite.modules.general.CosUtil;
import com.jeesite.modules.sys.utils.DictUtils;
import com.jeesite.modules.task.entity.Task;
import com.jeesite.modules.task.service.TaskService;
import com.jeesite.modules.taskrecord.entity.TaskRecord;
import com.jeesite.modules.taskrecord.service.TaskRecordService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "${adminPath}/home/home")
public class HomeController extends BaseController {

    @Autowired
    private HomeService homeService;

    @Autowired
    private PenalizeService penalizeService;

    @Autowired
    private TaskRecordService taskRecordService;

    @Autowired
    private ApplyService applyService;

    @Autowired
    private TaskService taskService;

    /**
     * 查询-我的待办-数据
     * @return
     */
    @RequestMapping(value = "findMyWaitHandle")
    public Page<Map<String, Object>> findMyWaitHandle(HttpServletRequest request){
        String taskName = request.getParameter("taskName");
        String memberCode = request.getParameter("memberCode");
        int pageNo = Integer.parseInt(request.getParameter("pageNo"));
        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
        return homeService.findMyWaitHandle(pageNo,pageSize,taskName,memberCode);
    }

    /**
     * 首页我的待办跳转后查询任务详情
     */
    @RequestMapping(value = "getTaskDetailData")
    public Map getTaskDetailData(String taskId){
        return homeService.getTaskDetailData(taskId);
    }

    /**
     * 分页查询待办任务
     */
    @RequestMapping(value = "findTaskPage")
    public Page<Map> findTaskPage(HttpServletRequest request, HttpServletResponse response){
        String taskName = request.getParameter("taskName");
        String memberCode = request.getParameter("memberCode");
        Page<Map> page = new Page<>(request, response);
        return homeService.findTaskPage(page,taskName,memberCode);
    }

    /**
     * 查询--任务发布--数据
     */
    @RequestMapping(value = "findTaskRelease")
    public Page<Task> findTaskRelease(Task task, HttpServletRequest request, HttpServletResponse response){
        task.setPage(new Page<>(request, response));
        return homeService.findTaskRelease(task);
    }

    /**
     * 任务审核
     */
    @RequestMapping(value = "findTaskRecord")
    public Page<Map> findTaskRecord(String nick,String examineState,String memberCode, HttpServletRequest request, HttpServletResponse response){
        Page<Map> page = new Page<>(request, response);
        Map map = new HashMap();
        map.put("nick",nick);
        map.put("examineState",examineState);
        map.put("memberCode",memberCode);
        return homeService.findTaskRecord(page,map);
    }

    /**
     * 用户登录账号
     */
    @RequestMapping(value = "getUserInfo")
    public Map getUserInfo(String nick,String password){
        Map userInfo = homeService.getUserInfo(nick, password);
        return userInfo;
    }

    /**
     * 查询公告任务数据
     */
    @RequestMapping(value = "findNotice")
    public Page<Map> findNotice(HttpServletRequest request, HttpServletResponse response){
        Page<Map> page = new Page<>(request, response);
        return homeService.findNotice(page);
    }

    /**
     * 轮播图图片链接
     */
    @RequestMapping(value = "findImgList")
    public List<Map> findImgList(){
        return homeService.findImgList();
    }

    /**
     * 修改密码
     */
    @RequestMapping(value = "updatePassword")
    @ResponseBody
    public String updatePassword(@Param("memberCode")String memberCode,
                                 @Param("nick")String nick,
                                 @Param("oldPassword")String oldPassword,
                                 @Param("newPassword")String newPassword){
        JSONObject res = homeService.updatePassword(memberCode, nick, oldPassword, newPassword);
        return renderResult(res.getString("res"),res.getString("msg"));
    }

    /**
     * 查询三条公告信息，展示到首页公告
     */
    @RequestMapping(value = "findThreeNoticeList")
    public List<Map> findThreeNoticeList(){
        return homeService.findThreeNoticeList();
    }

    /**
     * 获取字典数据
     */
    @RequestMapping(value = "findDictList")
    public List<Map> findDictList(String dictName){
        return homeService.findDictList(dictName);
    }

    /**
     * 任务发布--保存数据
     */
    @RequestMapping(value = "saveTaskData")
    @ResponseBody
    public String saveTaskData(@RequestParam(value = "file") MultipartFile file, Task task){
        if(file == null){
            return renderResult(Global.FALSE, text("未提交活动封面！"));
        }
        //上传封面图片
        JSONObject result = CosUtil.uploadFile(file, "", "xh/task/cover");
        if(!result.getBoolean("result")){
            task.setTaskImg(result.getString("fileName2"));
            taskService.save(task);
            return renderResult(Global.TRUE, text("保存数据成功！"));
        }else {
            taskService.save(task);
            return renderResult(Global.TRUE, text("封面图片上传失败，数据保存成功！"));
        }
    }

    /**
     * 上传图片文件辅助材料
     */
    @RequestMapping(value = "uploadFile")
    @ResponseBody
    public String uploadFile(@RequestParam(value = "file") MultipartFile file, String preFix){
        if(file == null){
            return renderResult(Global.FALSE, text("未提交文件！"));
        }
        JSONObject result = CosUtil.uploadFile(file, "", preFix);
        if(!result.getBoolean("result")){
            result.remove("result");
            return renderResult(Global.TRUE, text("上传成功！"),result);
        }else {
            return renderResult(Global.FALSE, text("上传失败！"));
        }
    }

    /**
     * 保存负面扣分数据
     */
    @PostMapping(value = "savePenalize")
    @ResponseBody
    public String savePenalize(Penalize penalize) {
        penalizeService.save(penalize);
        return renderResult(Global.TRUE, text("保存负面扣分成功！"));
    }


    /**
     * 基本信息修改
     */
    @RequestMapping(value = "updateMember")
    @ResponseBody
    public String updateMemberInfo(Member member){
        homeService.updateMemberInfo(member);
        return renderResult(Global.TRUE,text("修改用户信息成功"));
    }


    /**
     * 新增积分申请
     */
    @PostMapping(value = "saveIntegral")
    @ResponseBody
    public String saveIntegral(Apply apply) {
        homeService.saveIntegral(apply);
        return renderResult(Global.TRUE, text("积分申请成功！"));
    }

    /**
     * 查询积分（总数）
     */
    @RequestMapping(value = "findPointsTotal")
    public List<Map> findPointsTotal(String memberCode){
        return homeService.findPointsTotal(memberCode);
    }

    /**
     * 分页查询积分列表
     */
    @RequestMapping(value = "findPointsTotalPage")
    public Page<Map> findPointsTotalPage(String memberCode, HttpServletRequest request, HttpServletResponse response){
        Page<Map> page = new Page<>(request, response);
        Page<Map> pointsTotalPage = homeService.findPointsTotalPage(page, memberCode);
        return pointsTotalPage;
    }

    /**
     * 查询积分页面上半区数据
     */
    @RequestMapping(value = "getPointsData")
    public List<Map> getPointsData(String memberCode){
        return homeService.getPointsData(memberCode);
    }

    /**
     * 获取任务活动待审核信息
     */
    @RequestMapping(value = "getExamineInfo")
    @ResponseBody
    public String getExamineInfo(TaskRecord taskRecord){
        taskRecord = taskRecordService.get(taskRecord);
        taskRecord.setTaskType(DictUtils.getDictLabel("task_type",taskRecord.getTaskType(),"--"));
        String submitImg = taskRecord.getSubmitImg();
        Optional.ofNullable(submitImg).orElse("");
        String[] split = submitImg.split(",");
        for (int i = 0; i < split.length; i++) {
            split[i] = CosUtil.URL + split[i];
        }
        taskRecord.setSubmitImg(Arrays.stream(split).collect(Collectors.joining(",")));
        return renderResult(Global.TRUE, text("任务活动审核成功！"),taskRecord);
    }

    /**
     * 任务活动审核
     */
    @RequestMapping(value = "examineTask")
    @ResponseBody
    public String examineTask(TaskRecord taskRecord){
        taskRecord.setExamineDate(new Date());
        taskRecordService.save(taskRecord);
        return renderResult(Global.TRUE, text("任务活动审核成功！"));
    }

    /**
     * 保存任务活动数据
     */
    @RequestMapping(value = "saveTaskRecord")
    @ResponseBody
    public String saveTaskRecord(TaskRecord taskRecord){
        taskRecord.setExamineState("0");
        taskRecordService.insertTaskRecord(taskRecord);
        return renderResult(Global.TRUE, text("保存任务活动数据成功！"));
    }

    /**
     * 查询用户列表信息
     */
    @RequestMapping(value = "findUserInfoChange")
    public Page<Map> findUserInfoChange(String content,HttpServletRequest request, HttpServletResponse response){
        Page<Map> page = new Page<>(request, response);
        return homeService.findUserInfoChange(content,page);
    }

    /**
     * 获取申请列表
     */
    @RequestMapping(value = "findApplyRecord")
    @ResponseBody
    public Page<Apply> findApplyRecord(Apply apply, HttpServletRequest request, HttpServletResponse response) {
        apply.setPage(new Page<>(request, response));
        Page<Apply> page = applyService.findPage(apply);
        List<Apply> list = page.getList();
        for (Apply apply1 : list) {
            String materialImg = apply1.getMaterialImg();
            if(!StringUtils.isEmpty(materialImg)){
                String[] splitImg = Optional.ofNullable(materialImg).orElse("").split(",");
                for (int i = 0; i < splitImg.length; i++) {
                    splitImg[i] = CosUtil.URL + splitImg[i];
                }
                apply1.setMaterialImg(Arrays.stream(splitImg).collect(Collectors.joining(",")));
            }
            String materialFile = apply1.getMaterialFile();
            if(!StringUtils.isEmpty(materialFile)){
                String[] splitFile = Optional.ofNullable(materialFile).orElse("").split(",");
                for (int i = 0; i < splitFile.length; i++) {
                    splitFile[i] = CosUtil.URL + splitFile[i];
                }
                apply1.setMaterialFile(Arrays.stream(splitFile).collect(Collectors.joining(",")));
            }
        }
        return page;
    }

    /**
     * 查询已办任务数据
     */
    @RequestMapping(value = "getCompletedDetail")
    public Map getCompletedDetail(String taskId){
        return homeService.getCompletedDetail(taskId);
    }
}
