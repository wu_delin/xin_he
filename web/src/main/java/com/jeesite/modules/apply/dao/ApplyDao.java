package com.jeesite.modules.apply.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.apply.entity.Apply;

/**
 * 积分申请DAO接口
 * @author qu
 * @version 2023-11-06
 */
@MyBatisDao
public interface ApplyDao extends CrudDao<Apply> {
	
}