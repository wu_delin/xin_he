package com.jeesite.modules.apply.service;

import java.util.Date;
import java.util.List;
import com.jeesite.modules.member.dao.MemberDao;
import com.jeesite.modules.points.dao.PointsDao;
import com.jeesite.modules.points.entity.Points;
import com.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.apply.entity.Apply;
import com.jeesite.modules.apply.dao.ApplyDao;

/**
 * 积分申请Service
 * @author qu
 * @version 2023-11-06
 */
@Service
public class ApplyService extends CrudService<ApplyDao, Apply> {
    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private MemberDao memberDao;
	/**
	 * 获取单条数据
	 * @param apply
	 * @return
	 */
	@Override
	public Apply get(Apply apply) {
		return super.get(apply);
	}
	
	/**
	 * 查询分页数据
	 * @param apply 查询条件
	 * @param apply page 分页对象
	 * @return
	 */
	@Override
	public Page<Apply> findPage(Apply apply) {
		return super.findPage(apply);
	}
	
	/**
	 * 查询列表数据
	 * @param apply
	 * @return
	 */
	@Override
	public List<Apply> findList(Apply apply) {
		return super.findList(apply);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param apply
	 */
	@Override
	@Transactional
	public void save(Apply apply) {
		apply.setReviewBy(UserUtils.getUser().getUserCode());
		apply.setReviewDate(new Date());
        super.save(apply);
        if ("1".equals(apply.getApplyState())) {
            Points points = new Points();
            points.setMemberCode(apply.getCreateBy());
            points.setPointsNum(apply.getReviewFraction());
            points.setPointsType("1");
            points.setRelationId(apply.getApplyId());
            pointsDao.insert(points);
            memberDao.updatePoints(points);
        }
	}

}