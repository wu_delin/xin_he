package com.jeesite.modules.apply.entity;

import javax.validation.constraints.Size;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.sys.entity.User;

/**
 * 积分申请Entity
 * @author qu
 * @version 2023-11-06
 */
@Table(name="xh_apply", alias="a", label="积分申请信息", columns={
		@Column(name="apply_id", attrName="applyId", label="申请id", isPK=true),
		@Column(name="apply_title", attrName="applyTitle", label="标题", queryType=QueryType.LIKE),
		@Column(name="apply_content", attrName="applyContent", label="内容"),
		@Column(name="apply_type", attrName="applyType", label="申请类型"),
		@Column(name="apply_state", attrName="applyState", label="申请状态",isQuery = false),
		@Column(name="material_img", attrName="materialImg", label="申请材料图片"),
		@Column(name="material_file", attrName="materialFile", label="申请材料文件"),
		@Column(name="create_date", attrName="createDate", label="申请时间", isUpdate=false),
		@Column(name="create_by", attrName="createBy", label="申请者"),
		@Column(name="apply_fraction", attrName="applyFraction", label="申请分数"),
		@Column(name="review_by", attrName="reviewBy", label="审核者"),
		@Column(name="review_content", attrName="reviewContent", label="审核内容"),
		@Column(name="review_date", attrName="reviewDate", label="审核时间", isUpdateForce=true),
		@Column(name="review_fraction", attrName="reviewFraction", label="审核分数"),
	},joinTable={
		@JoinTable(type=Type.LEFT_JOIN, entity= Member.class, alias="b",
				on="a.create_by = b.member_code", attrName="this",
				columns={
						@Column(name="member_name", attrName="memberName", label="申请者姓名", queryType=QueryType.LIKE),
				}),
		@JoinTable(type=Type.LEFT_JOIN, entity= User.class, alias="u",
				on="a.review_by = u.user_code", attrName="this",
				columns={
						@Column(name="ref_name", attrName="refName", label="审核者姓名", queryType=QueryType.LIKE),
				}),
}, orderBy="a.apply_id DESC"
)
public class Apply extends DataEntity<Apply> {
	
	private static final long serialVersionUID = 1L;
	private String applyId;		// 申请id
	private String applyTitle;		// 标题
	private String applyContent;		// 内容
	private String applyType;		// 申请类型
	private String applyState;		// 申请状态
	private String materialImg;		// 申请材料图片
	private String materialFile;	// 申请材料文件
	private String reviewBy;		// 审核者
	private Date reviewDate;		// 审核时间
	private String applyFraction;		// 申请分数
	private String reviewFraction;		// 审核分数
	private String reviewContent;		// 审核内容
	private String memberName;		// 申请者姓名
	private String refName;		// 审核者姓名
	private String deleteImg;		// 删除图片
	private String deleteFile;		// 删除文件

	public Apply() {
		this(null);
	}
	
	public Apply(String id){
		super(id);
	}

    public String getDeleteImg() {
        return deleteImg;
    }

    public void setDeleteImg(String deleteImg) {
        this.deleteImg = deleteImg;
    }

    public String getDeleteFile() {
        return deleteFile;
    }

    public void setDeleteFile(String deleteFile) {
        this.deleteFile = deleteFile;
    }

    public String getMaterialImg() {
		return materialImg;
	}

	public void setMaterialImg(String materialImg) {
		this.materialImg = materialImg;
	}

	public String getMaterialFile() {
		return materialFile;
	}

	public void setMaterialFile(String materialFile) {
		this.materialFile = materialFile;
	}

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
	
	@Size(min=0, max=64, message="标题长度不能超过 64 个字符")
	public String getApplyTitle() {
		return applyTitle;
	}

	public void setApplyTitle(String applyTitle) {
		this.applyTitle = applyTitle;
	}
	
	@Size(min=0, max=500, message="内容长度不能超过 500 个字符")
	public String getApplyContent() {
		return applyContent;
	}

	public void setApplyContent(String applyContent) {
		this.applyContent = applyContent;
	}
	
	@Size(min=0, max=1, message="申请类型长度不能超过 1 个字符")
	public String getApplyType() {
		return applyType;
	}

	public void setApplyType(String applyType) {
		this.applyType = applyType;
	}
	
	@Size(min=0, max=1, message="申请状态长度不能超过 1 个字符")
	public String getApplyState() {
		return applyState;
	}

	public void setApplyState(String applyState) {
		this.applyState = applyState;
	}
	
	@Size(min=0, max=64, message="审核者长度不能超过 64 个字符")
	public String getReviewBy() {
		return reviewBy;
	}

	public void setReviewBy(String reviewBy) {
		this.reviewBy = reviewBy;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}
	
	@Size(min=0, max=6, message="申请分数长度不能超过 6 个字符")
	public String getApplyFraction() {
		return applyFraction;
	}

	public void setApplyFraction(String applyFraction) {
		this.applyFraction = applyFraction;
	}
	
	@Size(min=0, max=6, message="审核分数长度不能超过 6 个字符")
	public String getReviewFraction() {
		return reviewFraction;
	}

	public void setReviewFraction(String reviewFraction) {
		this.reviewFraction = reviewFraction;
	}

	public String getReviewContent() {
		return reviewContent;
	}

	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getRefName() {
		return refName;
	}

	public void setRefName(String refName) {
		this.refName = refName;
	}
}