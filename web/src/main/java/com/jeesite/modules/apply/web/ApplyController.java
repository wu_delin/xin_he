package com.jeesite.modules.apply.web;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.general.CosUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.apply.entity.Apply;
import com.jeesite.modules.apply.service.ApplyService;

/**
 * 积分申请Controller
 * @author qu
 * @version 2023-11-06
 */
@Controller
@RequestMapping(value = "${adminPath}/apply/apply")
public class ApplyController extends BaseController {

	@Autowired
	private ApplyService applyService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Apply get(String applyId, boolean isNewRecord) {
		return applyService.get(applyId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("apply:apply:view")
	@RequestMapping(value = {"list", ""})
	public String list(Apply apply, Model model) {
		model.addAttribute("apply", apply);
		return "modules/apply/applyList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("apply:apply:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Apply> listData(Apply apply, HttpServletRequest request, HttpServletResponse response) {
		apply.setPage(new Page<>(request, response));
		Page<Apply> page = applyService.findPage(apply);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("apply:apply:view")
	@RequestMapping(value = "form")
	public String form(Apply apply, Model model) {
		String materialImg = apply.getMaterialImg();
		materialImg = Optional.ofNullable(materialImg).orElse("");
        if("".equals(materialImg)){
            model.addAttribute("materialImg", new String[]{});
        }else {
            String[] imgs = materialImg.split(",");
            if(imgs.length>0){
                for (int i = 0; i < imgs.length; i++) {
                    imgs[i] = CosUtil.URL + imgs[i];
                }
                model.addAttribute("materialImg", imgs);
            }else {
                model.addAttribute("materialImg", imgs);
            }
        }
		String materialFile = apply.getMaterialFile();
		materialFile = Optional.ofNullable(materialFile).orElse("");
		if("".equals(materialFile)){
            model.addAttribute("materialFile", new String[]{});
        }else {
            String[] files = materialFile.split(",");
            if(files.length>0){
                for (int i = 0; i < files.length; i++) {
                    files[i] = CosUtil.URL + files[i];
                }
                model.addAttribute("materialFile", files);
            }else {
                model.addAttribute("materialFile", files);
            }
        }
		model.addAttribute("apply", apply);
		return "modules/apply/applyForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("apply:apply:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Apply apply) {
		applyService.save(apply);
		return renderResult(Global.TRUE, text("保存积分申请成功！"));
	}
	
	/**
	 * 删除数据
	 */
	@RequiresPermissions("apply:apply:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Apply apply) {
		applyService.delete(apply);
		return renderResult(Global.TRUE, text("删除积分申请成功！"));
	}
	
}