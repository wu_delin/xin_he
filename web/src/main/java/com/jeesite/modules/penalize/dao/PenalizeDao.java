package com.jeesite.modules.penalize.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.penalize.entity.Penalize;

/**
 * 负面扣分惩罚DAO接口
 * @author Mr Wu
 * @version 2023-11-03
 */
@MyBatisDao
public interface PenalizeDao extends CrudDao<Penalize> {
	
}