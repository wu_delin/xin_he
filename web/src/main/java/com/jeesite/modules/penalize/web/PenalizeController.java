package com.jeesite.modules.penalize.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.penalize.entity.Penalize;
import com.jeesite.modules.penalize.service.PenalizeService;

/**
 * 负面扣分惩罚Controller
 * @author Mr Wu
 * @version 2023-11-03
 */
@Controller
@RequestMapping(value = "${adminPath}/penalize/penalize")
public class PenalizeController extends BaseController {

	@Autowired
	private PenalizeService penalizeService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Penalize get(String recordId, boolean isNewRecord) {
		return penalizeService.get(recordId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("penalize:penalize:view")
	@RequestMapping(value = {"list", ""})
	public String list(Penalize penalize, Model model) {
		model.addAttribute("penalize", penalize);
		return "modules/penalize/penalizeList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("penalize:penalize:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Penalize> listData(Penalize penalize, HttpServletRequest request, HttpServletResponse response) {
		penalize.setPage(new Page<>(request, response));
		Page<Penalize> page = penalizeService.findPage(penalize);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("penalize:penalize:view")
	@RequestMapping(value = "form")
	public String form(Penalize penalize, Model model) {
		model.addAttribute("penalize", penalize);
		return "modules/penalize/penalizeForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("penalize:penalize:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Penalize penalize) {
		penalizeService.save(penalize);
		return renderResult(Global.TRUE, text("保存负面扣分成功！"));
	}
	
	/**
	 * 取消负面扣分
	 */
	@RequiresPermissions("penalize:penalize:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Penalize penalize) {
        penalize.setStatus("1");
		penalizeService.update(penalize);
		return renderResult(Global.TRUE, text("取消负面扣分成功！"));
	}
	
}