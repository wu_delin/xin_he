package com.jeesite.modules.penalize.entity;

import javax.validation.constraints.Size;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.sys.entity.User;

/**
 * 负面扣分惩罚Entity
 * @author Mr Wu
 * @version 2023-11-03
 */
@Table(name="xh_penalize", alias="a", label="负面扣分惩罚信息", columns={
		@Column(name="record_id", attrName="recordId", label="record_id", isPK=true),
		@Column(name="member_code", attrName="memberCode", label="用户code"),
		@Column(name="penalize_type", attrName="penalizeType", label="处罚类型"),
		@Column(name="penalize_detail", attrName="penalizeDetail", label="处罚详情"),
		@Column(name="points_num", attrName="pointsNum", label="扣除积分"),
		@Column(name="remove_reason", attrName="removeReason", label="取消惩罚原因"),
		@Column(name="status", attrName="status", label="状态【0正常1取消】"),
		@Column(name="create_by", attrName="createBy", label="create_by", isUpdate=false),
		@Column(name="create_date", attrName="createDate", label="create_date", isUpdate=false),
		@Column(name="update_by", attrName="updateBy", label="update_by"),
		@Column(name="update_date", attrName="updateDate", label="update_date"),
	},joinTable = {
        @JoinTable(type= JoinTable.Type.LEFT_JOIN, entity= Member.class, attrName="this", alias="m",
                on="m.member_code = a.member_code", columns={
                @Column(name="member_name", attrName="memberName", label="用户名称", queryType = QueryType.LIKE),
        })
    }, orderBy="a.update_date DESC"
)
public class Penalize extends DataEntity<Penalize> {
	
	private static final long serialVersionUID = 1L;
	private String recordId;		// record_id
	private String memberCode;		// 用户code
	private String memberName;		// 用户名称
	private String penalizeType;		// 处罚类型
	private String penalizeDetail;		// 处罚详情
	private String pointsNum;		// 扣除积分
	private String removeReason;		// 取消惩罚原因

	public Penalize() {
		this(null);
	}
	
	public Penalize(String id){
		super(id);
	}

    public String getRemoveReason() {
        return removeReason;
    }

    public void setRemoveReason(String removeReason) {
        this.removeReason = removeReason;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	
	@Size(min=0, max=1, message="处罚类型长度不能超过 1 个字符")
	public String getPenalizeType() {
		return penalizeType;
	}

	public void setPenalizeType(String penalizeType) {
		this.penalizeType = penalizeType;
	}
	
	@Size(min=0, max=256, message="处罚详情长度不能超过 256 个字符")
	public String getPenalizeDetail() {
		return penalizeDetail;
	}

	public void setPenalizeDetail(String penalizeDetail) {
		this.penalizeDetail = penalizeDetail;
	}
	
	@Size(min=0, max=10, message="扣除积分长度不能超过 10 个字符")
	public String getPointsNum() {
		return pointsNum;
	}

	public void setPointsNum(String pointsNum) {
		this.pointsNum = pointsNum;
	}
	
}