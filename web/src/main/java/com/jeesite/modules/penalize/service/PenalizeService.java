package com.jeesite.modules.penalize.service;

import java.util.List;

import com.jeesite.modules.member.dao.MemberDao;
import com.jeesite.modules.points.dao.PointsDao;
import com.jeesite.modules.points.entity.Points;
import com.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.penalize.entity.Penalize;
import com.jeesite.modules.penalize.dao.PenalizeDao;

import javax.validation.constraints.Size;

/**
 * 负面扣分惩罚Service
 * @author Mr Wu
 * @version 2023-11-03
 */
@Service
public class PenalizeService extends CrudService<PenalizeDao, Penalize> {

    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private MemberDao memberDao;

	/**
	 * 获取单条数据
	 * @param penalize
	 * @return
	 */
	@Override
	public Penalize get(Penalize penalize) {
		return super.get(penalize);
	}
	
	/**
	 * 查询分页数据
	 * @param penalize 查询条件
	 * @param penalize page 分页对象
	 * @return
	 */
	@Override
	public Page<Penalize> findPage(Penalize penalize) {
		return super.findPage(penalize);
	}
	
	/**
	 * 查询列表数据
	 * @param penalize
	 * @return
	 */
	@Override
	public List<Penalize> findList(Penalize penalize) {
		return super.findList(penalize);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param penalize
	 */
	@Override
	@Transactional
	public void save(Penalize penalize) {
		super.save(penalize);
        Points points = new Points();
        points.setMemberCode(penalize.getMemberCode());
        points.setPointsNum("-"+penalize.getPointsNum());
        points.setPointsType("9");
        points.setRelationId(penalize.getRecordId());
        pointsDao.insert(points);
        memberDao.updatePoints(points);
	}
	
	/**
	 * 取消负面扣分
	 * @param penalize
	 */
	@Override
	@Transactional
	public void update(Penalize penalize) {
		super.update(penalize);
        Points points = new Points();
        points.setRelationId(penalize.getRecordId());
        points.setStatus("1");
        points.setUpdateBy(UserUtils.getUser().getUserCode());
        pointsDao.removePoints(points);
        String pointsNum = points.getPointsNum();
        double num = Double.valueOf(pointsNum);
        points.setPointsNum(String.valueOf(Math.abs(num)));
        memberDao.updatePoints(points);
	}

}