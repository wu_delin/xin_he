package com.jeesite.modules.slideshow.entity;

import javax.validation.constraints.Size;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 轮播图Entity
 * @author han
 * @version 2023-10-29
 */
@Table(name="xh_slideshow", alias="a", label="轮播图信息", columns={
		@Column(name="row_id", attrName="rowId", label="row_id", isPK=true),
		@Column(name="img_url", attrName="imgUrl", label="图片链接"),
		@Column(name="sort", attrName="sort", label="排序号"),
		@Column(name="status", attrName="status", label="状态", isUpdate=false),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false, isUpdateForce=true),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false, isUpdateForce=true),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
	}, orderBy="a.sort"
)
public class XhSlideshow extends DataEntity<XhSlideshow> {
	
	private static final long serialVersionUID = 1L;
	private String rowId;		// row_id
	private String imgUrl;		// 图片链接
	private String deleteImg;		// 删除图片的名称
	private Integer sort;          //排序号
	public XhSlideshow() {
		this(null);
	}
	
	public XhSlideshow(String id){
		super(id);
	}
	
	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	
	@Size(min=0, max=64, message="图片链接长度不能超过 64 个字符")
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getDeleteImg() {
		return deleteImg;
	}

	public void setDeleteImg(String deleteImg) {
		this.deleteImg = deleteImg;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
}