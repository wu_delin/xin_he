package com.jeesite.modules.slideshow.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.slideshow.entity.XhSlideshow;

/**
 * 轮播图DAO接口
 * @author han
 * @version 2023-10-29
 */
@MyBatisDao
public interface XhSlideshowDao extends CrudDao<XhSlideshow> {
	
}