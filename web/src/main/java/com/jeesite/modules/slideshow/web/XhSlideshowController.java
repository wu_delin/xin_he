package com.jeesite.modules.slideshow.web;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.general.CosUtil;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.slideshow.entity.XhSlideshow;
import com.jeesite.modules.slideshow.service.XhSlideshowService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 轮播图Controller
 * @author han
 * @version 2023-10-29
 */
@Controller
@RequestMapping(value = "${adminPath}/slideshow/xhSlideshow")
public class XhSlideshowController extends BaseController {

	@Autowired
	private XhSlideshowService xhSlideshowService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public XhSlideshow get(String rowId, boolean isNewRecord) {
		return xhSlideshowService.get(rowId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("slideshow:xhSlideshow:view")
	@RequestMapping(value = {"list", ""})
	public String list(XhSlideshow xhSlideshow, Model model) {
		model.addAttribute("xhSlideshow", xhSlideshow);
		return "modules/slideshow/xhSlideshowList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("slideshow:xhSlideshow:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<XhSlideshow> listData(XhSlideshow xhSlideshow, HttpServletRequest request, HttpServletResponse response) {
		xhSlideshow.setPage(new Page<>(request, response));
		Page<XhSlideshow> page = xhSlideshowService.findPage(xhSlideshow);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("slideshow:xhSlideshow:view")
	@RequestMapping(value = "form")
	public String form(XhSlideshow xhSlideshow, Model model) {
		xhSlideshow.setImgUrl(CosUtil.URL+xhSlideshow.getImgUrl());
		model.addAttribute("xhSlideshow", xhSlideshow);
		return "modules/slideshow/xhSlideshowForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("slideshow:xhSlideshow:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@RequestParam(value = "files", required = false) MultipartFile[] files, @Validated XhSlideshow xhSlideshow) {
		if(xhSlideshow.getIsNewRecord()){
			if (!Objects.isNull(files)) {
				JSONObject result = CosUtil.uploadFile(files[0], "", "xh/slide");
				if(!result.getBoolean("result")){
					xhSlideshow.setImgUrl(result.getString("fileName2"));
					xhSlideshowService.insert(xhSlideshow);
					return renderResult(Global.TRUE, text("保存数据成功！"));
				}else {
					xhSlideshowService.insert(xhSlideshow);
					return renderResult(Global.TRUE, text("封面图片上传失败！"));
				}
			} else {
				return renderResult(Global.FALSE, text("封面图片未上传！"));
			}
		}else {
			if (!Objects.isNull(files)) {
				JSONObject result = CosUtil.uploadFile(files[0], "", "xh/slide");
				if(!result.getBoolean("result")){
					if (!StringUtils.isEmpty(xhSlideshow.getDeleteImg())) {
						CosUtil.delCOSFile(xhSlideshow.getDeleteImg(),"xh/task/cover");
					}
					xhSlideshow.setImgUrl(result.getString("fileName2"));
					xhSlideshowService.update(xhSlideshow);
					return renderResult(Global.TRUE, text("更新数据成功！"));
				}else {
					xhSlideshowService.update(xhSlideshow);
					return renderResult(Global.TRUE, text("封面图片上传失败！"));
				}
			}
			xhSlideshowService.update(xhSlideshow);
			return renderResult(Global.TRUE, text("更新数据成功！"));
		}
	}
	
	/**
	 * 停用数据
	 */
	@RequiresPermissions("slideshow:xhSlideshow:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(XhSlideshow xhSlideshow) {
		xhSlideshow.setStatus(XhSlideshow.STATUS_DISABLE);
		xhSlideshowService.updateStatus(xhSlideshow);
		return renderResult(Global.TRUE, text("停用轮播图成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequiresPermissions("slideshow:xhSlideshow:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(XhSlideshow xhSlideshow) {
		xhSlideshow.setStatus(XhSlideshow.STATUS_NORMAL);
		xhSlideshowService.updateStatus(xhSlideshow);
		return renderResult(Global.TRUE, text("启用轮播图成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequiresPermissions("slideshow:xhSlideshow:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(XhSlideshow xhSlideshow) {
		xhSlideshowService.delete(xhSlideshow);
		return renderResult(Global.TRUE, text("删除轮播图成功！"));
	}

	/**
	 * 轮播图排序
	 */
	@RequestMapping(value = "sort")
	@ResponseBody
	public String sort(HttpServletRequest request) {
		String params = request.getParameter("params");
		List<XhSlideshow> list = JSON.parseArray(params,XhSlideshow.class);
		xhSlideshowService.updateBatch(list);
		return renderResult(Global.TRUE, text("轮播图排序成功！"));
	}
	
}