package com.jeesite.modules.slideshow.service;

import java.util.List;

import com.jeesite.modules.general.CosUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.slideshow.entity.XhSlideshow;
import com.jeesite.modules.slideshow.dao.XhSlideshowDao;

/**
 * 轮播图Service
 * @author han
 * @version 2023-10-29
 */
@Service
public class XhSlideshowService extends CrudService<XhSlideshowDao, XhSlideshow> {
	
	/**
	 * 获取单条数据
	 * @param xhSlideshow
	 * @return
	 */
	@Override
	public XhSlideshow get(XhSlideshow xhSlideshow) {
		return super.get(xhSlideshow);
	}
	
	/**
	 * 查询分页数据
	 * @param xhSlideshow 查询条件
	 * @param xhSlideshow page 分页对象
	 * @return
	 */
	@Override
	public Page<XhSlideshow> findPage(XhSlideshow xhSlideshow) {
		return super.findPage(xhSlideshow);
	}
	
	/**
	 * 查询列表数据
	 * @param xhSlideshow
	 * @return
	 */
	@Override
	public List<XhSlideshow> findList(XhSlideshow xhSlideshow) {
		List<XhSlideshow> list = super.findList(xhSlideshow);
		for (XhSlideshow slide : list) {
			slide.setImgUrl(CosUtil.URL+slide.getImgUrl());
		}
		return list;
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param xhSlideshow
	 */
	@Override
	@Transactional
	public void save(XhSlideshow xhSlideshow) {
		super.save(xhSlideshow);
	}

	/**
	 * 批量修改
	 * @param xhSlideshows
	 */
	@Transactional
	public void updateBatch(List<XhSlideshow> xhSlideshows) {
		dao.updateBatch(xhSlideshows);
	}
	
	/**
	 * 更新状态
	 * @param xhSlideshow
	 */
	@Override
	@Transactional
	public void updateStatus(XhSlideshow xhSlideshow) {
		super.updateStatus(xhSlideshow);
	}
	
	/**
	 * 删除数据
	 * @param xhSlideshow
	 */
	@Override
	@Transactional
	public void delete(XhSlideshow xhSlideshow) {
		super.delete(xhSlideshow);
	}
	
}