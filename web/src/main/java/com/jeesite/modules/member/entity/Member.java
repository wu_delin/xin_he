package com.jeesite.modules.member.entity;

import javax.validation.constraints.Size;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 用户信息表Entity
 * @author qu
 * @version 2023-11-01
 */
@Table(name="xh_member", alias="a", label="用户信息表信息", columns={
		@Column(name="member_code", attrName="memberCode", label="用户编码", isPK=true),
		@Column(name="card_no", attrName="cardNo", label="身份证号"),
		@Column(name="member_name", attrName="memberName", label="用户名称", queryType=QueryType.LIKE),
		@Column(name="nick", attrName="nick", label="登陆账号", queryType=QueryType.LIKE),
		@Column(name="password", attrName="password", label="密码"),
		@Column(name="gender", attrName="gender", label="性别"),
		@Column(name="role", attrName="role", label="角色"),
		@Column(name="phone", attrName="phone", label="手机号"),
		@Column(name="birthday", attrName="birthday", label="生日", isUpdateForce=true),
		@Column(name="culture", attrName="culture", label="文化程度"),
		@Column(name="occupation", attrName="occupation", label="职业"),
		@Column(name="position_level", attrName="positionLevel", label="职称"),
		@Column(name="branch", attrName="branch", label="所属支部"),
		@Column(name="join_party_date", attrName="joinPartyDate", label="入党时间", isUpdateForce=true),
		@Column(name="duties", attrName="duties", label="担任职务"),
		@Column(name="reward_punish", attrName="rewardPunish", label="奖惩情况"),
		@Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false, isUpdateForce=true),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false, isUpdateForce=true),
		@Column(name="status", attrName="status", label="状态", isUpdate=false),
	}, orderBy="a.update_date DESC"
)
public class Member extends DataEntity<Member> {
	
	private static final long serialVersionUID = 1L;
	private String memberCode;		// 用户编码
	private String memberName;		// 用户名称
	private String nick;		// 登陆账号
	private String password;		// 密码
	private String gender;		// 性别
	private String role;		// 角色
	private String phone;		// 手机号
	private Date birthday;		// 生日
	private String culture;		// 文化程度
	private String occupation;		// 职业
	private String positionLevel;		// 职称
	private String branch;		// 所属支部
	private Date joinPartyDate;		// 入党时间
	private String duties;		// 担任职务
	private String rewardPunish;		// 奖惩情况
	private String cardNo;		// 身份证号
	private String integralTotal;		// 总积分

	public Member() {
		this(null);
	}
	
	public Member(String id){
		super(id);
	}

    public String getIntegralTotal() {
        return integralTotal;
    }

    public void setIntegralTotal(String integralTotal) {
        this.integralTotal = integralTotal;
    }

    public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
	@Size(min=0, max=64, message="用户名称长度不能超过 64 个字符")
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	@Size(min=0, max=64, message="登陆账号长度不能超过 64 个字符")
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	@Size(min=0, max=128, message="密码长度不能超过 128 个字符")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Size(min=0, max=1, message="性别长度不能超过 1 个字符")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Size(min=0, max=11, message="手机号长度不能超过 11 个字符")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	@Size(min=0, max=64, message="文化程度长度不能超过 64 个字符")
	public String getCulture() {
		return culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}
	
	@Size(min=0, max=64, message="职业长度不能超过 64 个字符")
	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	@Size(min=0, max=64, message="职称长度不能超过 64 个字符")
	public String getPositionLevel() {
		return positionLevel;
	}

	public void setPositionLevel(String positionLevel) {
		this.positionLevel = positionLevel;
	}
	
	@Size(min=0, max=64, message="所属支部长度不能超过 64 个字符")
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getJoinPartyDate() {
		return joinPartyDate;
	}

	public void setJoinPartyDate(Date joinPartyDate) {
		this.joinPartyDate = joinPartyDate;
	}
	
	@Size(min=0, max=64, message="担任职务长度不能超过 64 个字符")
	public String getDuties() {
		return duties;
	}

	public void setDuties(String duties) {
		this.duties = duties;
	}
	
	@Size(min=0, max=256, message="奖惩情况长度不能超过 256 个字符")
	public String getRewardPunish() {
		return rewardPunish;
	}

	public void setRewardPunish(String rewardPunish) {
		this.rewardPunish = rewardPunish;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}