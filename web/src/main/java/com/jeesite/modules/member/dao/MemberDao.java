package com.jeesite.modules.member.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.points.entity.Points;

import java.util.List;

/**
 * 用户信息表DAO接口
 * @author qu
 * @version 2023-11-01
 */
@MyBatisDao
public interface MemberDao extends CrudDao<Member> {
    void updatePoints(Points points);
    List<Member> findMemberList(Member member);
}