package com.jeesite.modules.member.web;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.codec.AesUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.member.service.MemberService;

/**
 * 用户信息表Controller
 * @author qu
 * @version 2023-11-01
 */
@Controller
@RequestMapping(value = "${adminPath}/member/member")
public class MemberController extends BaseController {

	@Autowired
	private MemberService memberService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Member get(String memberCode, boolean isNewRecord) {
		return memberService.get(memberCode, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("member:member:view")
	@RequestMapping(value = {"list", ""})
	public String list(Member member, Model model) {
		model.addAttribute("member", member);
		return "modules/member/memberList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("member:member:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Member> listData(Member member, HttpServletRequest request, HttpServletResponse response) {
		member.setPage(new Page<>(request, response));
		Page<Member> page = memberService.findPage(member);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("member:member:view")
	@RequestMapping(value = "form")
	public String form(Member member, Model model) {
		if (!Objects.isNull(member.getMemberCode())) {
			member.setCardNo(AesUtils.decode(member.getCardNo()));
			member.setPassword(AesUtils.decode(member.getPassword()));
			member.setPhone(AesUtils.decode(member.getPhone()));
		}
		model.addAttribute("member", member);
		return "modules/member/memberForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("member:member:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Member member) {
		memberService.save(member);
		return renderResult(Global.TRUE, text("保存用户信息表成功！"));
	}
	
	/**
	 * 停用数据
	 */
	@RequiresPermissions("member:member:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Member member) {
		member.setStatus(Member.STATUS_DISABLE);
		memberService.updateStatus(member);
		return renderResult(Global.TRUE, text("停用用户信息表成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequiresPermissions("member:member:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Member member) {
		member.setStatus(Member.STATUS_NORMAL);
		memberService.updateStatus(member);
		return renderResult(Global.TRUE, text("启用用户信息表成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequiresPermissions("member:member:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Member member) {
		memberService.delete(member);
		return renderResult(Global.TRUE, text("删除用户信息表成功！"));
	}

    /**
     * 用户选择
     */
    @RequestMapping(value = "memberSelect")
    public String memberSelect(Member member, Model model) {
        model.addAttribute("member", member);
        return "modules/member/memberSelect";
    }

    /**
     * 查询用户信息及积分数据
     */
    @RequestMapping(value = "findMemberList")
    @ResponseBody
    public Page<Member> findMemberList(Member member, HttpServletRequest request, HttpServletResponse response) {
        member.setPage(new Page<>(request, response));
        Page<Member> page = memberService.findMemberList(member);
        return page;
    }

}