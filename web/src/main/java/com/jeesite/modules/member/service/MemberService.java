package com.jeesite.modules.member.service;

import java.util.List;
import java.util.Objects;

import com.jeesite.common.codec.AesUtils;
import com.jeesite.common.codec.EncodeUtils;
import com.jeesite.common.codec.Sha1Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.member.entity.Member;
import com.jeesite.modules.member.dao.MemberDao;

/**
 * 用户信息表Service
 * @author qu
 * @version 2023-11-01
 */
@Service
public class MemberService extends CrudService<MemberDao, Member> {
	
	/**
	 * 获取单条数据
	 * @param member
	 * @return
	 */
	@Override
	public Member get(Member member) {
		return super.get(member);
	}
	
	/**
	 * 查询分页数据
	 * @param member 查询条件
	 * @param member page 分页对象
	 * @return
	 */
	@Override
	public Page<Member> findPage(Member member) {
		return super.findPage(member);
	}
	
	/**
	 * 查询列表数据
	 * @param member
	 * @return
	 */
	@Override
	public List<Member> findList(Member member) {
		if (!Objects.isNull(member.getPhone()) && !"".equals(member.getPhone())){
			member.setPhone(AesUtils.encode(member.getPhone()));
		}
		List<Member> list = super.findList(member);
		for (Member members : list) {
			members.setCardNo(AesUtils.decode(members.getCardNo()));
			members.setPassword(AesUtils.decode(members.getPassword()));
			members.setPhone(AesUtils.decode(members.getPhone()));
		}
		return list;
	}

	/**
	 * 保存数据（插入或更新）
	 * @param member
	 */
	@Override
	@Transactional
	public void save(Member member) {
		//密码、身份证号加密加密
		member.setCardNo(AesUtils.encode(member.getCardNo()));
		member.setPassword(AesUtils.encode(member.getPassword()));
		member.setPhone(AesUtils.encode(member.getPhone()));
		super.save(member);
	}
	
	/**
	 * 更新状态
	 * @param member
	 */
	@Override
	@Transactional
	public void updateStatus(Member member) {
		super.updateStatus(member);
	}
	
	/**
	 * 删除数据
	 * @param member
	 */
	@Override
	@Transactional
	public void delete(Member member) {
		super.delete(member);
	}

    /**
     * 查询用户信息及积分数据
     * @param member
     * @return
     */
    public Page<Member> findMemberList(Member member) {
        if (!Objects.isNull(member.getPhone()) && !"".equals(member.getPhone())){
            member.setPhone(AesUtils.encode(member.getPhone()));
        }
        List<Member> list = dao.findMemberList(member);
        for (Member members : list) {
            members.setCardNo(AesUtils.decode(members.getCardNo()));
            members.setPhone(AesUtils.decode(members.getPhone()));
        }
        Page<Member> page = member.getPage();
        page.setList(list);
        return page;
    }
}