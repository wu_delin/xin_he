package com.jeesite.modules.notice.web;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.general.CosUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.notice.entity.Notice;
import com.jeesite.modules.notice.service.NoticeService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 公告Controller
 * @author han
 * @version 2023-10-29
 */
@Controller
@RequestMapping(value = "${adminPath}/notice/notice")
public class NoticeController extends BaseController {

	@Autowired
	private NoticeService noticeService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Notice get(String rowId, boolean isNewRecord) {
		return noticeService.get(rowId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("notice:notice:view")
	@RequestMapping(value = {"list", ""})
	public String list(Notice notice, Model model) {
		model.addAttribute("notice", notice);
		return "modules/notice/noticeList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("notice:notice:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Notice> listData(Notice notice, HttpServletRequest request, HttpServletResponse response) {
		notice.setPage(new Page<>(request, response));
		Page<Notice> page = noticeService.findPage(notice);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("notice:notice:view")
	@RequestMapping(value = "form")
	public String form(Notice notice, Model model) {
        notice.setAnnexUrl(CosUtil.URL+notice.getAnnexUrl());
		model.addAttribute("notice", notice);
		return "modules/notice/noticeForm";
	}

	/**
	 * 保存数据
	 */
	@RequiresPermissions("notice:notice:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@RequestParam(value = "files", required = false) MultipartFile[] files, @Validated Notice notice) {
        if(notice.getIsNewRecord()){
            if (!Objects.isNull(files)) {
                JSONObject result = CosUtil.uploadFile(files[0], "", "xh/notice");
                if(!result.getBoolean("result")){
                    notice.setAnnexUrl(result.getString("fileName2"));
                    noticeService.insert(notice);
                    return renderResult(Global.TRUE, text("保存数据成功！"));
                }else {
                    noticeService.insert(notice);
                    return renderResult(Global.TRUE, text("附件上传失败！"));
                }
            } else {
                return renderResult(Global.FALSE, text("附件未上传！"));
            }
        }else {
            if (!Objects.isNull(files)) {
                JSONObject result = CosUtil.uploadFile(files[0], "", "xh/notice");
                if(!result.getBoolean("result")){
                    if (!StringUtils.isEmpty(notice.getDeleteAnnex())) {
                        CosUtil.delCOSFile(notice.getDeleteAnnex(),"xh/notice");
                    }
                    notice.setAnnexUrl(result.getString("fileName2"));
                    noticeService.update(notice);
                    return renderResult(Global.TRUE, text("更新数据成功！"));
                }else {
                    noticeService.update(notice);
                    return renderResult(Global.TRUE, text("附件上传失败！"));
                }
            }
            noticeService.update(notice);
            return renderResult(Global.TRUE, text("更新数据成功！"));
        }
	}
	
	/**
	 * 停用数据
	 */
	@RequiresPermissions("notice:notice:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Notice notice) {
		notice.setStatus(Notice.STATUS_DISABLE);
		noticeService.updateStatus(notice);
		return renderResult(Global.TRUE, text("停用公告成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequiresPermissions("notice:notice:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Notice notice) {
		notice.setStatus(Notice.STATUS_NORMAL);
		noticeService.updateStatus(notice);
		return renderResult(Global.TRUE, text("启用公告成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequiresPermissions("notice:notice:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Notice notice) {
		noticeService.delete(notice);
		return renderResult(Global.TRUE, text("删除公告成功！"));
	}
	
}