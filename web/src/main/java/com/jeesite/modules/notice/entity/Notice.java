package com.jeesite.modules.notice.entity;

import javax.validation.constraints.Size;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 公告Entity
 * @author han
 * @version 2023-10-29
 */
@Table(name="xh_notice", alias="a", label="公告信息", columns={
		@Column(name="row_id", attrName="rowId", label="row_id", isPK=true),
		@Column(name="intro", attrName="intro", label="公告标题", queryType=QueryType.LIKE),
		@Column(name="content", attrName="content", label="公告内容"),
		@Column(name="category", attrName="category", label="公告类别【0活动任务1附件公告】"),
		@Column(name="relation_id", attrName="relationId", label="关系id"),
		@Column(name="annex_name", attrName="annexName", label="附件名称"),
		@Column(name="annex_url", attrName="annexUrl", label="附件地址"),
		@Column(name="annex_size", attrName="annexSize", label="附件大小"),
		@Column(name="status", attrName="status", label="公告状态【0显示1隐藏】"),
        @Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
        @Column(name="create_by", attrName="createBy", label="创建者", isUpdate=false, isQuery=false),
        @Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
        @Column(name="update_by", attrName="updateBy", label="更新者",  isQuery=false),
	}, orderBy="a.row_id DESC"
)
public class Notice extends DataEntity<Notice> {
	
	private static final long serialVersionUID = 1L;
	private String rowId;		// row_id
	private String content;		// 公告内容
	private String intro;		// 公告标题
	private String category;		// 公告类别
	private String relationId;		// 关系id
	private String annexName;		// 附件名称
	private String annexUrl;		// 附件地址
	private String annexSize;		// 附件大小
	private String deleteAnnex;		// 待删除的附件


	public Notice() {
		this(null);
	}
	
	public Notice(String id){
		super(id);
	}

    public String getAnnexSize() {
        return annexSize;
    }

    public void setAnnexSize(String annexSize) {
        this.annexSize = annexSize;
    }

    public String getDeleteAnnex() {
        return deleteAnnex;
    }

    public void setDeleteAnnex(String deleteAnnex) {
        this.deleteAnnex = deleteAnnex;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	
	@Size(min=0, max=256, message="公告内容长度不能超过 256 个字符")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Size(min=0, max=64, message="关系id长度不能超过 64 个字符")
	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}
	
	@Size(min=0, max=256, message="附件名称长度不能超过 256 个字符")
	public String getAnnexName() {
		return annexName;
	}

	public void setAnnexName(String annexName) {
		this.annexName = annexName;
	}
	
	@Size(min=0, max=256, message="附件地址长度不能超过 256 个字符")
	public String getAnnexUrl() {
		return annexUrl;
	}

	public void setAnnexUrl(String annexUrl) {
		this.annexUrl = annexUrl;
	}
	
}