package com.jeesite.modules.notice.service;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.notice.entity.Notice;
import com.jeesite.modules.notice.dao.NoticeDao;

/**
 * 公告Service
 * @author han
 * @version 2023-10-29
 */
@Service
public class NoticeService extends CrudService<NoticeDao, Notice> {
	
	/**
	 * 获取单条数据
	 * @param notice
	 * @return
	 */
	@Override
	public Notice get(Notice notice) {
		return super.get(notice);
	}
	
	/**
	 * 查询分页数据
	 * @param notice 查询条件
	 * @param notice page 分页对象
	 * @return
	 */
	@Override
	public Page<Notice> findPage(Notice notice) {
		return super.findPage(notice);
	}
	
	/**
	 * 查询列表数据
	 * @param notice
	 * @return
	 */
	@Override
	public List<Notice> findList(Notice notice) {
		return super.findList(notice);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param notice
	 */
	@Override
	@Transactional
	public void save(Notice notice) {
		super.save(notice);
	}
	
	/**
	 * 更新状态
	 * @param notice
	 */
	@Override
	@Transactional
	public void updateStatus(Notice notice) {
		super.updateStatus(notice);
	}
	
	/**
	 * 删除数据
	 * @param notice
	 */
	@Override
	@Transactional
	public void delete(Notice notice) {
		super.delete(notice);
	}
	
}