package com.jeesite.modules.notice.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.notice.entity.Notice;

/**
 * 公告DAO接口
 * @author han
 * @version 2023-10-29
 */
@MyBatisDao
public interface NoticeDao extends CrudDao<Notice> {
	
}