package com.jeesite.modules.task.service;

import java.util.List;

import com.jeesite.modules.general.CosUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.task.entity.Task;
import com.jeesite.modules.task.dao.TaskDao;

/**
 * 任务Service
 * @author Mr Wu
 * @version 2023-10-25
 */
@Service
public class TaskService extends CrudService<TaskDao, Task> {
	
	/**
	 * 获取单条数据
	 * @param task
	 * @return
	 */
	@Override
	public Task get(Task task) {
        return super.get(task);
	}
	
	/**
	 * 查询分页数据
	 * @param task 查询条件
	 * @param task page 分页对象
	 * @return
	 */
	@Override
	public Page<Task> findPage(Task task) {
		return super.findPage(task);
	}
	
	/**
	 * 查询列表数据
	 * @param task
	 * @return
	 */
	@Override
	public List<Task> findList(Task task) {
        List<Task> list = super.findList(task);
        for (Task task1 : list) {
            task1.setTaskImg(CosUtil.URL+task1.getTaskImg());
        }
        return list;
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param task
	 */
	@Override
	@Transactional
	public void save(Task task) {
		super.save(task);
	}
	
	/**
	 * 更新状态
	 * @param task
	 */
	@Override
	@Transactional
	public void updateStatus(Task task) {
		super.updateStatus(task);
	}
	
	/**
	 * 删除数据
	 * @param task
	 */
	@Override
	@Transactional
	public void delete(Task task) {
		super.delete(task);
	}
	
}