package com.jeesite.modules.task.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.task.entity.Task;

/**
 * 任务DAO接口
 * @author Mr Wu
 * @version 2023-10-25
 */
@MyBatisDao
public interface TaskDao extends CrudDao<Task> {
	
}