package com.jeesite.modules.task.entity;

import javax.validation.constraints.Size;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 任务Entity
 * @author Mr Wu
 * @version 2023-10-25
 */
@Table(name="xh_task", alias="a", label="任务信息", columns={
		@Column(name="task_id", attrName="taskId", label="task_id", isPK=true),
		@Column(name="task_type", attrName="taskType", label="任务类型"),
		@Column(name="task_name", attrName="taskName", label="任务名称", queryType=QueryType.LIKE),
		@Column(name="task_intro", attrName="taskIntro", label="任务简介"),
		@Column(name="task_img", attrName="taskImg", label="任务封面图"),
		@Column(name="task_content", attrName="taskContent", label="任务内容"),
		@Column(name="start_date", attrName="startDate", label="开始时间", isUpdateForce=true),
		@Column(name="end_date", attrName="endDate", label="结束时间", isUpdateForce=true),
		@Column(name="task_points", attrName="taskPoints", label="任务积分"),
		@Column(name="leader_name", attrName="leaderName", label="负责人姓名", queryType=QueryType.LIKE),
		@Column(name="leader_phone", attrName="leaderPhone", label="负责人电话"),
		@Column(name="status", attrName="status", label="任务状态", isUpdate=false),
		@Column(name="create_date", attrName="createDate", label="发起时间", isUpdate=false, isQuery=false, isUpdateForce=true),
		@Column(name="create_by", attrName="createBy", label="发起者", isUpdate=false, isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false, isUpdateForce=true),
		@Column(name="update_by", attrName="updateBy", label="更新者", isQuery=false),
	}, orderBy="a.update_date DESC"
)
public class Task extends DataEntity<Task> {
	
	private static final long serialVersionUID = 1L;
	private String taskId;		// task_id
	private String taskType;		// 任务类型
	private String taskName;		// 任务名称
	private String taskIntro;		// 任务简介
	private String taskImg;		// 任务封面图
	private String taskContent;		// 任务内容
	private Date startDate;		// 开始时间
	private Date endDate;		// 结束时间
	private String taskPoints;		// 任务积分
	private String leaderName;		// 负责人姓名
	private String leaderPhone;		// 负责人电话
	private String deleteImg;		// 删除图片的名称

	public Task() {
		this(null);
	}
	
	public Task(String id){
		super(id);
	}

    public String getDeleteImg() {
        return deleteImg;
    }

    public void setDeleteImg(String deleteImg) {
        this.deleteImg = deleteImg;
    }

    public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	@Size(min=0, max=1, message="任务类型长度不能超过 1 个字符")
	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
	@Size(min=0, max=128, message="任务名称长度不能超过 128 个字符")
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	@Size(min=0, max=128, message="任务简介长度不能超过 256 个字符")
	public String getTaskIntro() {
		return taskIntro;
	}

	public void setTaskIntro(String taskIntro) {
		this.taskIntro = taskIntro;
	}

	public String getTaskImg() {
		return taskImg;
	}

	public void setTaskImg(String taskImg) {
		this.taskImg = taskImg;
	}
	
	@Size(min=0, max=256, message="任务内容长度不能超过 512 个字符")
	public String getTaskContent() {
		return taskContent;
	}

	public void setTaskContent(String taskContent) {
		this.taskContent = taskContent;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Size(min=0, max=5, message="任务积分长度不能超过 5 个字符")
	public String getTaskPoints() {
		return taskPoints;
	}

	public void setTaskPoints(String taskPoints) {
		this.taskPoints = taskPoints;
	}
	
	@Size(min=0, max=10, message="负责人姓名长度不能超过 10 个字符")
	public String getLeaderName() {
		return leaderName;
	}

	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	
	@Size(min=0, max=11, message="负责人电话长度不能超过 11 个字符")
	public String getLeaderPhone() {
		return leaderPhone;
	}

	public void setLeaderPhone(String leaderPhone) {
		this.leaderPhone = leaderPhone;
	}
	
}