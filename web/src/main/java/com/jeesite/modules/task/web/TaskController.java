package com.jeesite.modules.task.web;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.jeesite.common.idgen.IdGen;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.general.CosUtil;
import com.jeesite.modules.sys.entity.DictData;
import com.jeesite.modules.sys.utils.DictUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.task.entity.Task;
import com.jeesite.modules.task.service.TaskService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 任务Controller
 * @author Mr Wu
 * @version 2023-10-25
 */
@Controller
@RequestMapping(value = "${adminPath}/task/task")
public class TaskController extends BaseController {

	@Autowired
	private TaskService taskService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Task get(String taskId, boolean isNewRecord) {
		return taskService.get(taskId, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("task:task:view")
	@RequestMapping(value = {"list", ""})
	public String list(Task task, Model model) {
		model.addAttribute("task", task);
		return "modules/task/taskList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("task:task:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Task> listData(Task task, HttpServletRequest request, HttpServletResponse response) {
		task.setPage(new Page<>(request, response));
		Page<Task> page = taskService.findPage(task);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("task:task:view")
	@RequestMapping(value = "form")
	public String form(Task task, Model model) {
        task.setTaskImg(CosUtil.URL+task.getTaskImg());
		model.addAttribute("task", task);
		return "modules/task/taskForm";
	}



    /**
     * 保存活动任务数据和封面图片
     */
    @RequiresPermissions("task:task:edit")
    @PostMapping(value = "saveTask")
    @ResponseBody
    public String saveItem(@RequestParam(value = "files", required = false) MultipartFile[] files, @Validated Task task) {
        if(task.getIsNewRecord()){
            if (!Objects.isNull(files)) {
                JSONObject result = CosUtil.uploadFile(files[0], "", "xh/task/cover");
                if(!result.getBoolean("result")){
                    task.setTaskImg(result.getString("fileName2"));
                    taskService.insert(task);
                    return renderResult(Global.TRUE, text("保存数据成功！"));
                }else {
                    taskService.insert(task);
                    return renderResult(Global.TRUE, text("封面图片上传失败！"));
                }
            } else {
                return renderResult(Global.FALSE, text("封面图片未上传！"));
            }
        }else {
            if (!Objects.isNull(files)) {
                JSONObject result = CosUtil.uploadFile(files[0], "", "xh/task/cover");
                if(!result.getBoolean("result")){
                    if (!StringUtils.isEmpty(task.getDeleteImg())) {
                        CosUtil.delCOSFile(task.getDeleteImg(),"xh/task/cover");
                    }
                    task.setTaskImg(result.getString("fileName2"));
                    taskService.update(task);
                    return renderResult(Global.TRUE, text("更新数据成功！"));
                }else {
                    taskService.update(task);
                    return renderResult(Global.TRUE, text("封面图片上传失败！"));
                }
            }
            taskService.update(task);
            return renderResult(Global.TRUE, text("更新数据成功！"));
        }
    }

	/**
	 * 停用数据
	 */
	@RequiresPermissions("task:task:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Task task) {
		task.setStatus(Task.STATUS_DISABLE);
		taskService.updateStatus(task);
		return renderResult(Global.TRUE, text("停用任务成功"));
	}
	
	/**
	 * 启用数据
	 */
	@RequiresPermissions("task:task:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Task task) {
		task.setStatus(Task.STATUS_NORMAL);
		taskService.updateStatus(task);
		return renderResult(Global.TRUE, text("启用任务成功"));
	}
	
	/**
	 * 删除数据
	 */
	@RequiresPermissions("task:task:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Task task) {
		taskService.delete(task);
		return renderResult(Global.TRUE, text("删除任务成功！"));
	}
	
}